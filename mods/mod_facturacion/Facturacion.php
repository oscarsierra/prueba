<?php
require_once("../../conf/Configuracion.php");
require_once("../../jumichica/ConectorMSSQL.php");
require_once("../../mods/mod_usuarios/Usuario.php");
require_once("libs/general.php");

/**
 *
 */
class Facturacion {
	private $moduser;
	private $conector;
    function __construct() {
    	$this->moduser=new Usuario();
        $this->conector = new ConectorMSSQL(
        									Variables::$HOST_BD, 
        									Variables::$NOMBRE_BD, 
        									Variables::$USUARIO_BD, 
        									Variables::$CLAVE_BD
        									);
    }

    public function get_pagosventa($empresaid){
        $sql="  SELECT 	PunVenCod,BodCod,PunVenDes,PunVenDir,
                		PunVenTel,CenCosId,TerminalId
                FROM 	puntoventa 
                WHERE 	empresaid=".$empresaid;
        $datos=$this->conector->select($sql);
        $arr=array();
        for ($i=0; $i <count($datos) ; $i++) {
            $arr[]=array(
                'PunVenCod'  =>  trim($datos[$i][0]),
                'BodCod'	 =>  $datos[$i][1],
                'PunVenDes'  =>  $datos[$i][2],
                'PunVenDir'  =>  $datos[$i][3],
                'PunVenTel'  =>  $datos[$i][4],
                'CenCosId' 	 =>  $datos[$i][5],
                'TerminalId' =>  $datos[$i][6]
            );
        }
        $retorno=json_encode(array("status_code"=>"200","data"=>$arr));
        return $retorno;
    }

    public function get_centroscostos($empresaid){
        $sql="  SELECT 	CenCosId,CenCosNom
                FROM 	centroscosto
                WHERE 	empresaid=".$empresaid;
        $datos=$this->conector->select($sql);
        $arr=array();
        for ($i=0; $i <count($datos) ; $i++) {
            $arr[]=array(
                'CenCosId'	 =>  trim($datos[$i][0]),
                'CenCosNom'  =>  $datos[$i][1]
            );
        }
        $retorno=json_encode(array("status_code"=>"200","data"=>$arr));
        return $retorno;
    }

    public function get_vendedor_empresa($nitVendedor,$idEmpresa){
        $sql="  SELECT 	dn.empresaid,dn.dirnitid,dn.DirNitRazSoc,
		                dn.DirNitNom1,dn.DirNitNom2,dn.DirNitApe1,
		                dn.DirNitApe2,dn.DirNitDir,dn.DirNitTel,
		                dn.DirNitFax,dn.DirNitEmail
                FROM 	directorionits dn, vendedores v
                WHERE 	v.vencod=dn.dirnitid
                AND 	dn.empresaid=".$idEmpresa."
                AND 	dn.dirnitid=".$nitVendedor."
                AND 	dn.DirNitemp = 1";
        $datos=$this->conector->select($sql);
        $arr=array();

        if(count($datos)>0){
            $arr=array(
                'EmpresaId'		=>	$datos[0][0],
                'Dirnitid'		=>	$datos[0][1],
                'DirNitRazSoc'	=>	$datos[0][2],
                'DirNitNom1'	=>	$datos[0][3],
                'DirNitNom2'	=>	$datos[0][4],
                'DirNitApe1'	=>	$datos[0][5],
                'DirNitApe2'	=>	$datos[0][6],
                'DirNitDir'		=>	$datos[0][7],
                'DirNitTel'		=>	$datos[0][8],
                'DirNitFax'		=>	$datos[0][9],
                'DirNitEmail'	=>	$datos[0][10]
            );
        }
        
        $retorno=json_encode(array("status_code"=>"200","data"=>$arr));
        return $retorno;
    }

    
    public function get_vendedores($idEmpresa){
        $sql="  SELECT 	dn.empresaid,dn.dirnitid,dn.DirNitRazSoc,
		                dn.DirNitNom1,dn.DirNitNom2,dn.DirNitApe1,
		                dn.DirNitApe2,dn.DirNitDir,dn.DirNitTel,dn.DirNitFax,dn.DirNitEmail
                FROM 	directorionits dn, vendedores v
                WHERE 	v.vencod=dn.dirnitid
                AND 	dn.empresaid=".$idEmpresa."
                AND 	dn.DirNitemp = 1";
        $datos=$this->conector->select($sql);
        $arr=array();
        for ($i=0; $i <count($datos) ; $i++) {
            $arr[]=array(
                'EmpresaId'		=>	utf8_encode(trim($datos[$i][0])),
                'Dirnitid'		=>	utf8_encode(trim($datos[$i][1])),
                'DirNitRazSoc'	=>	utf8_encode(trim($datos[$i][2])),
                'DirNitNom1'	=>	utf8_encode(trim($datos[$i][3])),
                'DirNitNom2'	=>	utf8_encode(trim($datos[$i][4])),
                'DirNitApe1'	=>	utf8_encode(trim($datos[$i][5])),
                'DirNitApe2'	=>	utf8_encode(trim($datos[$i][6])),
                'DirNitDir'		=>	utf8_encode(trim($datos[$i][7])),
                'DirNitTel'		=>	utf8_encode(trim($datos[$i][8])),
                'DirNitFax'		=>	utf8_encode(trim($datos[$i][9])),
                'DirNitEmail'	=>	utf8_encode(trim($datos[$i][10]))
            );
        }
        $retorno=json_encode(array("status_code"=>"200","data"=>$arr));
        return $retorno;
    }

    public function get_cliente_empresa($nitCliente,$idEmpresa){
        $sql="  SELECT 	dn.empresaid,dn.dirnitid,dn.DirNitRazSoc,
		                dn.DirNitNom1,dn.DirNitNom2,dn.DirNitApe1,
		                dn.DirNitApe2,dn.DirNitDir,dn.DirNitTel,dn.DirNitFax,dn.DirNitEmail
                FROM 	directorionits dn
                WHERE 	dn.empresaid=".$idEmpresa."
                AND 	dn.dirnitid='".$nitCliente."'
                AND 	dn.DirNitCli = 1";

        $datos=$this->conector->select($sql);

        if(count($datos)==0){
            $retorno=json_encode(array("status_code"=>"200","msj"=>"No existe Cliente","data"=>""));
            return $retorno;
            die();
        }

        $arr=array();
            $arr=array(
                'EmpresaId'		=>	utf8_encode($datos[0][0]),
                'Dirnitid'		=>	utf8_encode($datos[0][1]),
                'DirNitRazSoc'	=>	utf8_encode($datos[0][2]),
                'DirNitNom1'	=>	utf8_encode($datos[0][3]),
                'DirNitNom2'	=>	utf8_encode($datos[0][4]),
                'DirNitApe1'	=>	utf8_encode($datos[0][5]),
                'DirNitApe2'	=>	utf8_encode($datos[0][6]),
                'DirNitDir'		=>	utf8_encode($datos[0][7]),
                'DirNitTel'		=>	utf8_encode($datos[0][8]),
                'DirNitFax'		=>	utf8_encode($datos[0][9]),
                'DirNitEmail'	=>	utf8_encode($datos[0][10])
            );

        $retorno=json_encode(array("status_code"=>"200","data"=>$arr));
        return $retorno;
    }

    public function get_servicios_productos($idEmpresa,$filtros,$parameters,$limit,$paginar,$order){
          $KEYS=array();
          $KEYS['RefCod']		=	array('key'=>'re.RefCod','tabla'=>true);
          $KEYS['RefDes']		=	array('key'=>'re.RefDes','tabla'=>true);
          $KEYS['RefMed']		=	array('key'=>'re.RefMed','tabla'=>true);
          $KEYS['RefStock']		=	array('key'=>'re.RefStock','tabla'=>true);
          $KEYS['RefValVen']	=	array('key'=>'re.RefValVen','tabla'=>true);
          $KEYS['ClaRefDes']	=	array('key'=>'cr.ClaRefDes','tabla'=>true);
          $KEYS['ClaRefPorIva']	=	array('key'=>'cr.ClaRefPorIva','tabla'=>true);
          $KEYS['LinRefIndSer'] =   array('key'=>'lr.LinRefIndSer','tabla'=>true);
          $KEYS['ClaRefTipImp'] =   array('key'=>'cr.ClaRefTipImp','tabla'=>true);
          $KEYS['ClaRefPorCom']	=	array('key'=>'ClaRefPorCom','tabla'=>false);

          $sqlselect="  SELECT  re.RefCod,re.RefDes,re.RefMed,re.RefStock,
                                re.RefValVen ,cr.ClaRefDes,cr.ClaRefPorIva,lr.LinRefIndSer,
                                cr.ClaRefTipImp
                        FROM 	referencias re ,clasesreferencia cr,lineareferencia lr";
          $sqlwhere="   WHERE 	re.clarefcod=cr.clarefcod
                        AND 	lr.linrefcod=re.linrefcod
                        AND 	lr.empresaid=re.empresaid
                        AND 	re.empresaid=cr.empresaid 
                        AND 	re.empresaid=".$idEmpresa;
          $sql=integrar_api_sql($sqlselect,$sqlwhere,$KEYS,$filtros,$limit,$order);
          $datos=$this->conector->select($sql);
          $arr=array();
          for ($i=0; $i < count($datos); $i++) {
            $ClaRefPorCom=0;
            $ClaRefPorIva=0;
            if(trim($datos[$i][8])==1){
                $ClaRefPorCom=$datos[$i][6];
            }else{
                $ClaRefPorIva=$datos[$i][6];
            }
            $arr[]=array(
                            'RefCod'		=>	utf8_encode(trim($datos[$i][0])),
                            'RefDes'		=>	utf8_encode(trim($datos[$i][1])),
                            'RefMed'		=>	utf8_encode(trim($datos[$i][2])),
                            'RefStock'		=>	utf8_encode(trim($datos[$i][3])),
                            'RefValVen'		=>	utf8_encode(trim($datos[$i][4])),
                            'ClaRefDes'		=>	utf8_encode(trim($datos[$i][5])),
                            'ClaRefPorIva'	=>	utf8_encode($ClaRefPorIva),
                            'LinRefIndSer'  =>  utf8_encode(trim($datos[$i][7])),
                            'ClaRefTipImp'	=>	utf8_encode(trim($datos[$i][8])),
                            'ClaRefPorCom'  =>  utf8_encode($ClaRefPorCom)
                        );
          }

          $arrparameters=json_decode($parameters,true);

          if(count($arrparameters)==0){
            foreach ($KEYS as $key => $value) {
                $arrparameters[]=$key;
            }
          }
          $response=filter_parameters($arr,$arrparameters);
          $response=page_data($response,json_decode($paginar,true));
          $respuesta=array('status_code'=>200,'data'=>$response) ;

          return json_encode($respuesta );
    }


    public function insertar_factura($Factura,$FacDet,$idEmpresa){
        $decodeFacDet=json_decode($FacDet);
        $decodeFactura=json_decode($Factura);

        $FacFecVen = $decodeFactura->vence;        
        $vendedor_id = $decodeFactura->vendedor_id;        
        $centro_costo_id = $decodeFactura->centro_costo_id;        
        $cliente_id = $decodeFactura->cliente_id;        
        $placa = $decodeFactura->placa;        
        $subtotal = $decodeFactura->subtotal;        
        $imp1 = $decodeFactura->imp1;        
        $imp2 = $decodeFactura->imp2;        
        $descuento = $decodeFactura->descuento;        
        $total = $decodeFactura->total;        
        $pagado = $decodeFactura->pagado;        
        $diferencia = $decodeFactura->diferencia;        
        $observaciones = $decodeFactura->observaciones;   
        $tipo = $decodeFactura->tipo;   
        $punto_pago = $decodeFactura->punto_pago;   
        $tipcomid = $decodeFactura->tipcomid;   
        $factura_id = $decodeFactura->factura_id;   
        $norden = $decodeFactura->norden;   

        $FacPerAgno=date("Y");
        $FacPerMes=date("m");
        $FacFec=date("Y-m-d");
        $FacUltConsRef=count($decodeFacDet);

        $datos=$this->conector->select("SELECT max(cast(FacNum AS int )) AS facnum 
                                        FROM factura
                                        WHERE empresaid=".$idEmpresa."
                                        AND TipComId='".$tipcomid."'");
        $FacNum = str_pad($datos[0][0]+1, 12, "0", STR_PAD_LEFT);

        $insertFacD = " INSERT INTO FACTURA 
                        (
                            FacNum,FacAnu,FacImp,FacIndFac,FacPerAgno,FacPerMes,FacFec,FacFecVen,VenCod,PunVenCod,
                            FacObs,FacPlaVeh,FacNumOrd,FacPorFle,FacValFle, FacIndDevTot,FacIndDevPar,FacPorDes, 
                            FacObse,FacValIvaObse,ForPagCod,FacForNumTar,FacForNomTar,BanCod,FacForNumChe,FacForValPag,
                            TipComIdDocGen,DocNumGen,FacFecDocGen,FacUltConsRef,FacPuntos,FacNumCom,ResCod,FacCenCos,
                            DirNitId,VehCod,EmpresaId,FacValRet,FacValRetIva,FacValRetIca,FacHora,
                            FacBodCod,FacCodSucursal,TipComId,UsuarioId,FacCuotas,FacValorPropina,FacObsDev,
                            UsuarioIdDev,FacRetefuenteDev,FacReteIcaDev,FacReteIvaDev,FacPorRetFte,
                            FacPorRetIva,FacPorRetIca,FacValCree,FacPorCree,FacCreeDev,FacIndCombustible,FacFinCuota,
                            FacFin,FacFecEnt,FacPorInt,FacValorDescuento
                        ) VALUES (
                            '".$FacNum."','0','0','0','".$FacPerAgno."','".$FacPerMes."', getdate(),".
                            "CONVERT(datetime,'".$FacFecVen."', 120),'".$vendedor_id."','".$punto_pago."','".$observaciones.
                            "','".$placa."','','0','0','0','0','".$descuento."','0','0','".$tipo."','','','','','".
                            $total."','','', getdate(),'". $FacUltConsRef."','0','','','".$centro_costo_id.
                            "','".$cliente_id."','','".$idEmpresa."','0','0','0',".
                            "getdate(),'000',NULL,'".
                            $tipcomid."','1','0',NULL,NULL,0,'0','0','0','0','0','0','0','0','0',
                             '0','0','0',getdate(),'0','0'
                        )";

        $this->conector->insert($insertFacD);
        $last_insert=$this->conector->select("SELECT SCOPE_IDENTITY() as last_insert");
        $FacturaId=$last_insert[0][0];
        $VALORESFALTAN=$subtotal."','".$imp1."','".$imp2."','".$descuento.$pagado."','".$diferencia;
        for ($i=0; $i < count($decodeFacDet); $i++) { 
            $insertFacDet = "INSERT INTO FACTURADETALLE
                                        (
                                            FacturaId,FacCons,RefCod,FacDetCant,FacDetValUni,FacDetPorDes,
                                            FacDetPorIva,FacDetCantDev,FacDetDetalle,FacDetKilos,FacDetValUniIva,
                                            FacDetExistencias,FacDetTotal,FacDetImpolicores,FacDetValorDes,FacDetFecVen
                                        ) VALUES (
                                            '".$FacturaId."','".($i+1)."','".$decodeFacDet[$i]->RefCod."','".
                                               $decodeFacDet[$i]->cantidad."','".$decodeFacDet[$i]->RefValVen."','".
                                               $decodeFacDet[$i]->descuento."','".$decodeFacDet[$i]->ClaRefPorIva
                                               ."','0','".$decodeFacDet[$i]->descripcion_servicio."','0','0','0','0','0','0','".
                                               $FacFecVen."'
                                        )";

            $this->conector->insert($insertFacDet);
        }

        $mod_fact = new Facturacion();
        $respuestaGetFac=$mod_fact->get_factura($idEmpresa,$FacturaId);
        $resFac=json_decode($respuestaGetFac);      
        $respuesta=array('status_code'=>200,'data'=>$resFac->data) ;
        return json_encode($respuesta );

    }
    public function facturar_factura($Factura,$FacDet,$idEmpresa){
    	$decodeFactura	=	json_decode($Factura);
		$tipcomid 		=	 $decodeFactura->tipcomid;   
		$norden 		=	 $decodeFactura->norden;  

        $mod_fact = new Facturacion();
        $respuestaGetFac = $mod_fact->insertar_factura($Factura,$FacDet,$idEmpresa);
        $resFac = json_decode($respuestaGetFac);      
		$FacturaId = $resFac->data->FacturaId;

		if(trim($norden)!=""){$FacNumOrd=$tipcomid."-".$norden;}else{$FacNumOrd="";}

        $this->conector->update("	UPDATE 	factura 
        							SET 	FacIndFac=1, FacNumOrd='".$FacNumOrd."'
        							WHERE 	facturaid='".$FacturaId."'");

        $resFac->data->FacIndFac=1;
        $resFac->data->FacNumOrd=$FacNumOrd;

        echo json_encode($resFac);
	}



    public function actualizar_factura($Factura,$FacDet,$idEmpresa,$FacId){
        $decodeFacDet=json_decode($FacDet);
        $decodeFactura=json_decode($Factura);

        $FacFecVen = $decodeFactura->vence;        
        $vendedor_id = $decodeFactura->vendedor_id;        
        $centro_costo_id = $decodeFactura->centro_costo_id;        
        $cliente_id = $decodeFactura->cliente_id;        
        $placa = $decodeFactura->placa;        
        $subtotal = $decodeFactura->subtotal;        
        $imp1 = $decodeFactura->imp1;        
        $imp2 = $decodeFactura->imp2;        
        $descuento = $decodeFactura->descuento;        
        $total = $decodeFactura->total;        
        $pagado = $decodeFactura->pagado;        
        $diferencia = $decodeFactura->diferencia;        
        $observaciones = $decodeFactura->observaciones;   
        $tipo = $decodeFactura->tipo;   
        $punto_pago = $decodeFactura->punto_pago;   
        $tipcomid = $decodeFactura->tipcomid;   

        $FacPerAgno=date("Y");
        $FacPerMes=date("m");
        $FacFec=date("Y-m-d");
        $FacUltConsRef=count($decodeFacDet);

        $insertFacD="	UPDATE 	FACTURA  
                     	SET 	FacPerAgno='".$FacPerAgno."',
	                     		FacPerMes='".$FacPerMes."',
	                     		FacFec=getdate(),
	                     		FacFecVen=CONVERT(datetime,'".$FacFecVen."', 120),
	                     		VenCod='".$vendedor_id."',
	                     		PunVenCod='".$punto_pago."',
	                     		FacObs='".$observaciones."',
	                     		FacPlaVeh='".$placa."',
	                     		ForPagCod='".$tipo."',
	                     		FacForValPag='".$total."',
	                     		FacFecDocGen=getdate(),
	                     		FacCenCos='".$centro_costo_id."',
	                     		FacUltConsRef='".$FacUltConsRef."',
	                     		DirNitId='".$cliente_id."',
	                     		EmpresaId='".$idEmpresa."',
	                     		FacHora=getdate(),
	                     		FacBodCod='000',
	                     		TipComId='".$tipcomid."',
	                     		UsuarioId='1',
	                     		FacFecEnt=getdate()
                    	WHERE 	facturaid='".$FacId."'";

        $this->conector->update($insertFacD);

        $deleteFac="DELETE FROM FACTURADETALLE WHERE FacturaId='".$FacId."'";
        $this->conector->update($deleteFac);
        
        $VALORESFALTAN=$subtotal."','".$imp1."','".$imp2."','".$descuento.$pagado."','".$diferencia;
        for ($i=0; $i < count($decodeFacDet); $i++) { 
            $insertFacDet = "INSERT INTO FACTURADETALLE
                                        (
                                            FacturaId,FacCons,RefCod,FacDetCant,FacDetValUni,FacDetPorDes,
                                            FacDetPorIva,FacDetCantDev,FacDetDetalle,FacDetKilos,FacDetValUniIva,
                                            FacDetExistencias,FacDetTotal,FacDetImpolicores,FacDetValorDes,FacDetFecVen
                                        ) VALUES (
                                            '".$FacId."','".($i+1)."','".$decodeFacDet[$i]->RefCod."','".
                                               $decodeFacDet[$i]->cantidad."','".$decodeFacDet[$i]->RefValVen."','".
                                               $decodeFacDet[$i]->descuento."','".$decodeFacDet[$i]->ClaRefPorIva
                                               ."','0','".$decodeFacDet[$i]->descripcion_servicio."','0','0','0','0','0','0','".
                                               $FacFecVen."'
                                        )";

            $this->conector->insert($insertFacDet);
        }

        $mod_fact = new Facturacion();
        $respuestaGetFac=$mod_fact->get_factura($idEmpresa,$FacId);
        $resFac=json_decode($respuestaGetFac);      
        $respuesta=array('status_code'=>200,'data'=>$resFac->data) ;
        return json_encode($respuesta );

    }


    public function get_factura($idEmpresa,$FacturaId){
        $sql="  SELECT  f.FacturaId,f.FacNum,f.FacAnu,f.FacImp,f.FacIndFac,f.FacPerAgno,
        				f.FacPerMes,f.FacFec,f.FacFecVen,f.VenCod,f.PunVenCod,f.FacObs,
        				f.FacPlaVeh,f.FacNumOrd,f.FacPorFle,f.FacValFle,f.FacIndDevTot,
        				f.FacIndDevPar,f.FacPorDes,f.FacObse,f.FacValIvaObse,f.ForPagCod,
        				f.FacForNumTar,f.FacForNomTar,f.BanCod,f.FacForNumChe,f.FacForValPag,
        				f.TipComIdDocGen,f.DocNumGen,f.FacFecDocGen,f.FacUltConsRef,
        				f.FacPuntos,f.FacNumCom,f.ResCod,f.FacCenCos,f.DirNitId,f.VehCod,
        				f.EmpresaId,f.FacValRet,f.FacValRetIva,f.FacValRetIca,f.FacHora,
        				f.FacBodCod,f.FacCodSucursal,f.TipComId,f.UsuarioId,f.FacCuotas,
        				f.FacValorPropina,f.FacObsDev,f.UsuarioIdDev,f.FacRetefuenteDev,
        				f.FacReteIcaDev,f.FacReteIvaDev,f.FacPorRetFte,f.FacPorRetIva,
        				f.FacPorRetIca,f.FacValCree,f.FacPorCree,f.FacCreeDev,f.FacIndCombustible,
        				f.FacFinCuota,f.FacFin,f.FacFecEnt,f.FacPorInt,f.FacValorDescuento,dn.DirNitRazSoc
				FROM 	FACTURA f, directorionits dn
				WHERE 	f.DirNitId=dn.DirNitId
				AND 	f.empresaid=dn.empresaid
				AND 	f.FacturaId='".$FacturaId."'
				AND 	f.empresaid='".$idEmpresa."'";
        $datos=$this->conector->select($sql);

        if(count($datos)==0){
            $retorno=json_encode(array("status_code"=>"400","msj"=>"No existe factura"));
            return $retorno;
            die();
        }

        $sql2=" SELECT  fd.FacturaId,fd.FacCons,fd.RefCod,fd.FacDetCant,fd.FacDetValUni,
    					fd.FacDetPorDes,fd.FacDetPorIva,fd.FacDetCantDev,fd.FacDetDetalle,
    					fd.FacDetKilos,fd.FacDetValUniIva,fd.FacDetExistencias,
                        fd.FacDetTotal,fd.FacDetImpolicores,fd.FacDetValorDes,
                        fd.FacDetFecVen,r.RefDes,r.RefMed,r.RefStock,r.RefValVen ,
                        cr.ClaRefDes,cr.ClaRefPorIva,lr.LinRefIndSer,cr.ClaRefTipImp
                FROM 	FACTURADETALLE fd, referencias r,clasesreferencia cr,lineareferencia lr
                WHERE  	fd.RefCod=r.RefCod
                AND 	r.clarefcod=cr.clarefcod
                AND 	lr.linrefcod=r.linrefcod
                AND 	lr.empresaid=r.empresaid
                AND 	r.empresaid=cr.empresaid 
                AND 	FacturaId='".$FacturaId."'";
        $datos2=$this->conector->select($sql2);

        $FacDtealle=array();
        for ($i=0; $i < count($datos2) ; $i++) {

                        $ClaRefPorCom=0;
            $ClaRefPorIva=0;
            if(trim($datos2[$i][23])==1){
                $ClaRefPorCom=$datos2[$i][21];
            }else{
                $ClaRefPorIva=$datos2[$i][21];
            }

            $FacDtealle[]=array(
                'FacturaId'			=>$datos2[$i][0],
                'FacCons'			=>$datos2[$i][1],
                'RefCod'			=>$datos2[$i][2],
                'FacDetCant'		=>$datos2[$i][3],
                'FacDetValUni'		=>$datos2[$i][4],
                'FacDetPorDes'		=>$datos2[$i][5],
                'FacDetPorIva'		=>$datos2[$i][6],
                'FacDetCantDev'		=>$datos2[$i][7],
                'FacDetDetalle'		=>$datos2[$i][8],
                'FacDetKilos'		=>$datos2[$i][9],
                'FacDetValUniIva'	=>$datos2[$i][10],
                'FacDetExistencias'	=>$datos2[$i][11],
                'FacDetTotal'		=>$datos2[$i][12],
                'FacDetImpolicores'	=>$datos2[$i][13],
                'FacDetValorDes'	=>$datos2[$i][14],
                'FacDetFecVen' 		=>$datos2[$i][15],
                'RefDes' 			=>$datos2[$i][16],
                'RefMed' 			=>$datos2[$i][17],
                'RefStock' 			=>$datos2[$i][18],
                'RefValVen' 		=>$datos2[$i][19],
                'ClaRefDes' 		=>$datos2[$i][20],
                'ClaRefPorIva' 		=>utf8_encode($ClaRefPorIva),
                'LinRefIndSer' 		=>$datos2[$i][22],
                'ClaRefPorCom'  =>  utf8_encode($ClaRefPorCom)

            );
        }




        $arr=array(
            'FacturaId'			=>	$datos[0][0],
            'FacNum'			=>	$datos[0][1],
            'FacAnu'			=>	$datos[0][2],
            'FacImp'			=>	$datos[0][3],
            'FacIndFac'			=>	$datos[0][4],
            'FacPerAgno'		=>	$datos[0][5],
            'FacPerMes'			=>	$datos[0][6],
            'FacFec'			=>	$datos[0][7],
            'FacFecVen'			=>	date("Y-m-d",strtotime($datos[0][8])),
            'VenCod'			=>	$datos[0][9],
            'PunVenCod'			=>	$datos[0][10],
            'FacObs'			=>	$datos[0][11],
            'FacPlaVeh'			=>	$datos[0][12],
            'FacNumOrd'			=>	$datos[0][13],
            'FacPorFle'			=>	$datos[0][14],
            'FacValFle'			=>	$datos[0][15],
            'FacIndDevTot'		=>	$datos[0][16],
            'FacIndDevPar'		=>	$datos[0][17],
            'FacPorDes'			=>	$datos[0][18],
            'FacObse'			=>	$datos[0][19],
            'FacValIvaObse'		=>	$datos[0][20],
            'ForPagCod'			=>	$datos[0][21],
            'FacForNumTar'		=>	$datos[0][22],
            'FacForNomTar'		=>	$datos[0][23],
            'BanCod'			=>	$datos[0][24],
            'FacForNumChe'		=>	$datos[0][25],
            'FacForValPag'		=>	$datos[0][26],
            'TipComIdDocGen'	=>	$datos[0][27],
            'DocNumGen'			=>	$datos[0][28],
            'FacFecDocGen'		=>	$datos[0][29],
            'FacUltConsRef'		=>	$datos[0][30],
            'FacPuntos'			=>	$datos[0][31],
            'FacNumCom'			=>	$datos[0][32],
            'ResCod'			=>	$datos[0][33],
            'FacCenCos'			=>	$datos[0][34],
            'DirNitId'			=>	$datos[0][35],
            'VehCod'			=>	$datos[0][36],
            'EmpresaId'			=>	$datos[0][37],
            'FacValRet'			=>	$datos[0][38],
            'FacValRetIva'		=>	$datos[0][39],
            'FacValRetIca'		=>	$datos[0][40],
            'FacHora'			=>	$datos[0][41],
            'FacBodCod'			=>	$datos[0][42],
            'FacCodSucursal'	=>	$datos[0][43],
            'TipComId'			=>	$datos[0][44],
            'UsuarioId'			=>	$datos[0][45],
            'FacCuotas'			=>	$datos[0][46],
            'FacValorPropina'	=>	$datos[0][47],
            'FacObsDev'			=>	$datos[0][48],
            'UsuarioIdDev'		=>	$datos[0][49],
            'FacRetefuenteDev'	=>	$datos[0][50],
            'FacReteIcaDev'		=>	$datos[0][51],
            'FacReteIvaDev'		=>	$datos[0][52],
            'FacPorRetFte'		=>	$datos[0][53],
            'FacPorRetIva'		=>	$datos[0][54],
            'FacPorRetIca'		=>	$datos[0][55],
            'FacValCree'		=>	$datos[0][56],
            'FacPorCree'		=>	$datos[0][57],
            'FacCreeDev'		=>	$datos[0][58],
            'FacIndCombustible'	=>	$datos[0][59],
            'FacFinCuota'		=>	$datos[0][60],
            'FacFin'			=>	$datos[0][61],
            'FacFecEnt'			=>	$datos[0][62],
            'FacPorInt'			=>	$datos[0][63],
            'FacValorDescuento'	=>	$datos[0][64],
            'DirNitRazSoc' 		=>	$datos[0][65],
            'FacDetalle'		=>	$FacDtealle
            
        );

        $retorno=json_encode(array("status_code"=>"200","data"=>$arr));
        return $retorno;
    }

    public function get_facturas($idEmpresa,$filtros,$parameters,$limit,$paginar,$order){

        $KEYS=array();
        $KEYS['FacturaId']			=	array('key'=>'f.FacturaId','tabla'=>true);
        $KEYS['FacNum']				=	array('key'=>'f.FacNum','tabla'=>true);
        $KEYS['FacAnu']				=	array('key'=>'f.FacAnu','tabla'=>true);
        $KEYS['FacImp']				=	array('key'=>'f.FacImp','tabla'=>true);
        $KEYS['FacIndFac']			=	array('key'=>'f.FacIndFac','tabla'=>true);
        $KEYS['FacPerAgno']			=	array('key'=>'f.FacPerAgno','tabla'=>true);
        $KEYS['FacPerMes']			=	array('key'=>'f.FacPerMes','tabla'=>true);
        $KEYS['FacFec']				=	array('key'=>'f.FacFec','tabla'=>true);
        $KEYS['FacFecVen']			=	array('key'=>'f.FacFecVen','tabla'=>true);
        $KEYS['VenCod']				=	array('key'=>'f.VenCod','tabla'=>true);
        $KEYS['PunVenCod']			=	array('key'=>'f.PunVenCod','tabla'=>true);
        $KEYS['FacObs']				=	array('key'=>'f.FacObs','tabla'=>true);
        $KEYS['FacPlaVeh']			=	array('key'=>'f.FacPlaVeh','tabla'=>true);
        $KEYS['FacNumOrd']			=	array('key'=>'f.FacNumOrd','tabla'=>true);
        $KEYS['FacPorFle']			=	array('key'=>'f.FacPorFle','tabla'=>true);
        $KEYS['FacValFle']			=	array('key'=>'f.FacValFle','tabla'=>true);
        $KEYS['FacIndDevTot']		=	array('key'=>'f.FacIndDevTot','tabla'=>true);
        $KEYS['FacIndDevPar']		=	array('key'=>'f.FacIndDevPar','tabla'=>true);
        $KEYS['FacPorDes']			=	array('key'=>'f.FacPorDes','tabla'=>true);
        $KEYS['FacObse']			=	array('key'=>'f.FacObse','tabla'=>true);
        $KEYS['FacValIvaObse']		=	array('key'=>'f.FacValIvaObse','tabla'=>true);
        $KEYS['ForPagCod']			=	array('key'=>'f.ForPagCod','tabla'=>true);
        $KEYS['FacForNumTar']		=	array('key'=>'f.FacForNumTar','tabla'=>true);
        $KEYS['FacForNomTar']		=	array('key'=>'f.FacForNomTar','tabla'=>true);
        $KEYS['BanCod']				=	array('key'=>'f.BanCod','tabla'=>true);
        $KEYS['FacForNumChe']		=	array('key'=>'f.FacForNumChe','tabla'=>true);
        $KEYS['FacForValPag']		=	array('key'=>'f.FacForValPag','tabla'=>true);
        $KEYS['TipComIdDocGen']		=	array('key'=>'f.TipComIdDocGen','tabla'=>true);
        $KEYS['DocNumGen']			=	array('key'=>'f.DocNumGen','tabla'=>true);
        $KEYS['FacFecDocGen']		=	array('key'=>'f.FacFecDocGen','tabla'=>true);
        $KEYS['FacUltConsRef']		=	array('key'=>'f.FacUltConsRef','tabla'=>true);
        $KEYS['FacPuntos']			=	array('key'=>'f.FacPuntos','tabla'=>true);
        $KEYS['FacNumCom']			=	array('key'=>'f.FacNumCom','tabla'=>true);
        $KEYS['ResCod']				=	array('key'=>'f.ResCod','tabla'=>true);
        $KEYS['FacCenCos']			=	array('key'=>'f.FacCenCos','tabla'=>true);
        $KEYS['DirNitId']			=	array('key'=>'f.DirNitId','tabla'=>true);
        $KEYS['VehCod']				=	array('key'=>'f.VehCod','tabla'=>true);
        $KEYS['EmpresaId']			=	array('key'=>'f.EmpresaId','tabla'=>true);
        $KEYS['FacValRet']			=	array('key'=>'f.FacValRet','tabla'=>true);
        $KEYS['FacValRetIva']		=	array('key'=>'f.FacValRetIva','tabla'=>true);
        $KEYS['FacValRetIca']		=	array('key'=>'f.FacValRetIca','tabla'=>true);
        $KEYS['FacHora']			=	array('key'=>'f.FacHora','tabla'=>true);
        $KEYS['FacBodCod']			=	array('key'=>'f.FacBodCod','tabla'=>true);
        $KEYS['FacCodSucursal']		=	array('key'=>'f.FacCodSucursal','tabla'=>true);
        $KEYS['TipComId']			=	array('key'=>'f.TipComId','tabla'=>true);
        $KEYS['UsuarioId']			=	array('key'=>'f.UsuarioId','tabla'=>true);
        $KEYS['FacCuotas']			=	array('key'=>'f.FacCuotas','tabla'=>true);
        $KEYS['FacValorPropina']	=	array('key'=>'f.FacValorPropina','tabla'=>true);
        $KEYS['FacObsDev']			=	array('key'=>'f.FacObsDev','tabla'=>true);
        $KEYS['UsuarioIdDev']		=	array('key'=>'f.UsuarioIdDev','tabla'=>true);
        $KEYS['FacRetefuenteDev']	=	array('key'=>'f.FacRetefuenteDev','tabla'=>true);
        $KEYS['FacReteIcaDev']	 	=	array('key'=>'f.FacReteIcaDev','tabla'=>true);
        $KEYS['FacReteIvaDev']		=	array('key'=>'f.FacReteIvaDev','tabla'=>true);
        $KEYS['FacPorRetFte']		=	array('key'=>'f.FacPorRetFte','tabla'=>true);
        $KEYS['FacPorRetIva']		=	array('key'=>'f.FacPorRetIva','tabla'=>true);
        $KEYS['FacPorRetIca']		=	array('key'=>'f.FacPorRetIca','tabla'=>true);
        $KEYS['FacValCree']			=	array('key'=>'f.FacValCree','tabla'=>true);
        $KEYS['FacPorCree']			=	array('key'=>'f.FacPorCree','tabla'=>true);
        $KEYS['FacCreeDev']			=	array('key'=>'f.FacCreeDev','tabla'=>true);
        $KEYS['FacIndCombustible']	=	array('key'=>'f.FacIndCombustible','tabla'=>true);
        $KEYS['FacFinCuota']		=	array('key'=>'f.FacFinCuota','tabla'=>true);
        $KEYS['FacFin']				=	array('key'=>'f.FacFin','tabla'=>true);
        $KEYS['FacFecEnt']			=	array('key'=>'f.FacFecEnt','tabla'=>true);
        $KEYS['FacPorInt']			=	array('key'=>'f.FacPorInt','tabla'=>true);
        $KEYS['FacValorDescuento']	=	array('key'=>'f.FacValorDescuento','tabla'=>true);
        $KEYS['DirNitRazSoc']		=	array('key'=>'dn.DirNitRazSoc','tabla'=>true);

          $sqlselect="  SELECT  f.FacturaId,f.FacNum,f.FacAnu,f.FacImp,f.FacIndFac,f.FacPerAgno,
          						f.FacPerMes,f.FacFec,f.FacFecVen,f.VenCod,f.PunVenCod,f.FacObs,
          						f.FacPlaVeh,f.FacNumOrd,f.FacPorFle,f.FacValFle,f.FacIndDevTot,
          						f.FacIndDevPar,f.FacPorDes,f.FacObse,f.FacValIvaObse,f.ForPagCod,
          						f.FacForNumTar,f.FacForNomTar,f.BanCod,f.FacForNumChe,f.FacForValPag,
          						f.TipComIdDocGen,f.DocNumGen,f.FacFecDocGen,f.FacUltConsRef,
          						f.FacPuntos,f.FacNumCom,f.ResCod,f.FacCenCos,f.DirNitId,f.VehCod,
          						f.EmpresaId,f.FacValRet,f.FacValRetIva,f.FacValRetIca,f.FacHora,
          						f.FacBodCod,f.FacCodSucursal,f.TipComId,f.UsuarioId,f.FacCuotas,
          						f.FacValorPropina,f.FacObsDev,f.UsuarioIdDev,f.FacRetefuenteDev,
          						f.FacReteIcaDev,f.FacReteIvaDev,f.FacPorRetFte,f.FacPorRetIva,
          						f.FacPorRetIca,f.FacValCree,f.FacPorCree,f.FacCreeDev,
          						f.FacIndCombustible,f.FacFinCuota,f.FacFin,f.FacFecEnt,
          						f.FacPorInt,f.FacValorDescuento,dn.DirNitRazSoc
                        FROM 	FACTURA f,directorionits dn";
          $sqlwhere="   WHERE 	f.DirNitId=dn.DirNitId
                        AND 	f.EmpresaId=dn.EmpresaId
                        AND 	f.EmpresaId=".$idEmpresa;



          $sql=integrar_api_sql($sqlselect,$sqlwhere,$KEYS,$filtros,$limit,$order,json_decode($paginar,true));

          $tempCount=$this->conector->select("select count(*) from (".$_POST['sql'].") tempCount");

          $datos=$this->conector->select($sql);

          $arr=array();
          for ($i=0; $i < count($datos); $i++) {
            $arr[]=array(
                            'FacturaId'			=>	utf8_encode($datos[$i][0]),
                            'FacNum'			=>	utf8_encode($datos[$i][1]),
                            'FacAnu'			=>	utf8_encode($datos[$i][2]),
                            'FacImp'			=>	utf8_encode($datos[$i][3]),
                            'FacIndFac'			=>	utf8_encode($datos[$i][4]),
                            'FacPerAgno'		=>	utf8_encode($datos[$i][5]),
                            'FacPerMes'			=>	utf8_encode($datos[$i][6]),
                            'FacFec'			=>	utf8_encode($datos[$i][7]),
                            'FacFecVen'			=>	utf8_encode(date("Y-m-d",strtotime($datos[$i][8]))),
                            'VenCod'			=>	utf8_encode($datos[$i][9]),
                            'PunVenCod'			=>	utf8_encode($datos[$i][10]),
                            'FacObs'			=>	utf8_encode($datos[$i][11]),
                            'FacPlaVeh'			=>	utf8_encode($datos[$i][12]),
                            'FacNumOrd'			=>	utf8_encode($datos[$i][13]),
                            'FacPorFle'			=>	utf8_encode($datos[$i][14]),
                            'FacValFle'			=>	utf8_encode($datos[$i][15]),
                            'FacIndDevTot'		=>	utf8_encode($datos[$i][16]),
                            'FacIndDevPar'		=>	utf8_encode($datos[$i][17]),
                            'FacPorDes'			=>	utf8_encode($datos[$i][18]),
                            'FacObse'			=>	utf8_encode($datos[$i][19]),
                            'FacValIvaObse'		=>	utf8_encode($datos[$i][20]),
                            'ForPagCod'			=>	utf8_encode($datos[$i][21]),
                            'FacForNumTar'		=>	utf8_encode($datos[$i][22]),
                            'FacForNomTar'		=>	utf8_encode($datos[$i][23]),
                            'BanCod'			=>	utf8_encode($datos[$i][24]),
                            'FacForNumChe'		=>	utf8_encode($datos[$i][25]),
                            'FacForValPag'		=>	utf8_encode($datos[$i][26]),
                            'TipComIdDocGen'	=>	utf8_encode($datos[$i][27]),
                            'DocNumGen'			=>	utf8_encode($datos[$i][28]),
                            'FacFecDocGen'		=>	utf8_encode(date("Y-m-d h:m a",strtotime($datos[$i][29]))),
                            'FacUltConsRef'		=>	utf8_encode($datos[$i][30]),
                            'FacPuntos'			=>	utf8_encode($datos[$i][31]),
                            'FacNumCom'			=>	utf8_encode($datos[$i][32]),
                            'ResCod'			=>	utf8_encode($datos[$i][33]),
                            'FacCenCos'			=>	utf8_encode($datos[$i][34]),
                            'DirNitId'			=>	utf8_encode($datos[$i][35]),
                            'VehCod'			=>	utf8_encode($datos[$i][36]),
                            'EmpresaId'			=>	utf8_encode($datos[$i][37]),
                            'FacValRet'			=>	utf8_encode($datos[$i][38]),
                            'FacValRetIva'		=>	utf8_encode($datos[$i][39]),
                            'FacValRetIca'		=>	utf8_encode($datos[$i][40]),
                            'FacHora'			=>	utf8_encode($datos[$i][41]),
                            'FacBodCod'			=>	utf8_encode($datos[$i][42]),
                            'FacCodSucursal'	=>	utf8_encode($datos[$i][43]),
                            'TipComId'			=>	utf8_encode($datos[$i][44]),
                            'UsuarioId'			=>	utf8_encode($datos[$i][45]),
                            'FacCuotas'			=>	utf8_encode($datos[$i][46]),
                            'FacValorPropina'	=>	utf8_encode($datos[$i][47]),
                            'FacObsDev'			=>	utf8_encode($datos[$i][48]),
                            'UsuarioIdDev'		=>	utf8_encode($datos[$i][49]),
                            'FacRetefuenteDev'	=>	utf8_encode($datos[$i][50]),
                            'FacReteIcaDev'		=>	utf8_encode($datos[$i][51]),
                            'FacReteIvaDev'		=>	utf8_encode($datos[$i][52]),
                            'FacPorRetFte'		=>	utf8_encode($datos[$i][53]),
                            'FacPorRetIva'		=>	utf8_encode($datos[$i][54]),
                            'FacPorRetIca'		=>	utf8_encode($datos[$i][55]),
                            'FacValCree'		=>	utf8_encode($datos[$i][56]),
                            'FacPorCree'		=>	utf8_encode($datos[$i][57]),
                            'FacCreeDev'		=>	utf8_encode($datos[$i][58]),
                            'FacIndCombustible'	=>	utf8_encode($datos[$i][59]),
                            'FacFinCuota'		=>	utf8_encode($datos[$i][60]),
                            'FacFin'			=>	utf8_encode($datos[$i][61]),
                            'FacFecEnt'			=>	utf8_encode($datos[$i][62]),
                            'FacPorInt'			=>	utf8_encode($datos[$i][63]),
                            'FacValorDescuento'	=>	utf8_encode($datos[$i][64]),
                            'DirNitRazSoc'		=>	utf8_encode($datos[$i][65])
                        );
          }

          $arrparameters=json_decode($parameters,true);

          if(count($arrparameters)==0){
            foreach ($KEYS as $key => $value) {
                $arrparameters[]=$key;
            }
          }
          $response=filter_parameters($arr,$arrparameters);

          //$response=page_data($response,json_decode($paginar,true));

          if(isset($tempCount[0][0])){
            $respuesta=array('status_code'=>200,'data'=>$response,'cantidad'=>$tempCount[0][0]) ;
          }else{
            $respuesta=array('status_code'=>200,'data'=>$response) ;

          }


          return json_encode($respuesta );

    }


    public function get_tiposcomprobante($idEmpresa){
        $sql=" SELECT 	tipcomid,tipcomnom
               FROM 	tipocomprobante
               WHERE  	empresaid='".$idEmpresa."'";
        $datos=$this->conector->select($sql);

        $arr=array();
        for ($i=0; $i < count($datos) ; $i++) {
            $arr[]=array(
                'tipcomid'=>$datos[$i][0],
                'tipcomnom'=>$datos[$i][1]
            );
        }

        $retorno=json_encode(array("status_code"=>"200","data"=>$arr));
        return $retorno;
    }

    public function insertar_directorio (	
	    									$idEmpresa,$DirNitId,$TipDocCod,$DirNitRazSoc,$DirNitNom1,
	                                        $DirNitNom2,$DirNitApe1,$DirNitApe2,$DirNitDir,
	                                        $DirNitTel,$DirNitFax,$DirNitEmail,$DepCod,
	                                        $MunCod,$TipoNit,$DirDeclarante
                                    	){
    	$DirNitCli=0;
    	$DirNitSoc=0;
    	$DirNitPro=0;
    	$DirNitemp=0;
    	switch ($TipoNit) {
    		case '1' : $DirNitCli = 1; break;
    		case '2' : $DirNitSoc = 1; break;
    		case '3' : $DirNitPro = 1; break;
    		case '4' : $DirNitemp = 1; break;
    	}

    	$insertDirNit="	INSERT INTO DIRECTORIONITS 
						(
							EmpresaId, DirNitId, DirNitDigVer,
							TipDocCod, DirNitRazSoc, DirNitNom1,
							DirNitNom2, DirNitApe1, DirNitApe2,
							DirNitCon, DirNitDir, DirNitTel,
							DirNitFax, DirNitEmail, DepCod,
							MunCod, BarCod, DirNitEst,
							DirNitReg, DirNitPro, DirNitAut,
							DirNitGra, DirNitRetIca, DirNitPorRetIca,
							DirNitCli, DirNitCliTip, DirNitCliCup,
							DirNitEmp, DirNitSoc, DirPreCom,
							DirNumCom, ZonCod, DirZonaEspecial,
							DirNitSobregiro, DirNitListaPrecios, DirNitVendedor,
							DirNitForPag, DirNitDirEntrega, DirNaturaleza,
							DirActEconomica, DirCodPais, DirDeclarante
						) VALUES(
							".$idEmpresa.", '".$DirNitId."', ' ',
							'".$TipDocCod."', '".$DirNitRazSoc."','".$DirNitNom1."',
							'".$DirNitNom2."', '".$DirNitApe1."', '".$DirNitApe2."',
							' ', '".$DirNitDir."', '".$DirNitTel."', 
							'".$DirNitFax."', '".$DirNitEmail."','".$DepCod."',
							'".$MunCod."', '00001', 1,
							1,".$DirNitPro.", 0,
							0, 0, 0,
							".$DirNitCli.", 'M', 0,
							".$DirNitemp.",".$DirNitSoc.",' ',
							0, '00', 0,
							NULL, NULL, NULL,
							NULL,NULL, NULL,
							NULL, NULL, ".$DirDeclarante."
						)";

		$this->conector->insert($insertDirNit);
        $retorno=json_encode(array("status_code"=>"200"));
        return $retorno;
    }


    public function get_tipo_documentos(){
        $sql="  SELECT TipDocCod,TipDocNom from tipodocumento";
        $datos=$this->conector->select($sql);
        $arr=array();
        for ($i=0; $i <count($datos) ; $i++) {
            $arr[]=array(
                'TipDocCod'  =>  utf8_encode(trim($datos[$i][0])),
                'TipDocNom'	 =>  utf8_encode($datos[$i][1])
            );
        }
        $retorno=json_encode(array("status_code"=>"200","data"=>$arr));
        return $retorno;
    }

    public function get_departamentos_empresa($idEmpresa){
        $sql="  SELECT DepCod,DepNombre 
        		from DEPARTAMENTOS
        		where EmpresaId=".$idEmpresa;
        $datos=$this->conector->select($sql);
        $arr=array();
        for ($i=0; $i <count($datos) ; $i++) {
            $arr[]=array(
                'DepCod'  =>  utf8_encode(trim($datos[$i][0])),
                'DepNombre'	 =>  utf8_encode($datos[$i][1])
            );
        }
        $retorno=json_encode(array("status_code"=>"200","data"=>$arr));
        return $retorno;
    }

    public function get_municipios_departamento($idEmpresa,$idDepto){
        $sql="  SELECT MunCod,MunNom 
        		from DEPARTAMENTOSMUNICIPIOS
        		where EmpresaId=".$idEmpresa.
        		"AND DepCod='".$idDepto."'";
        $datos=$this->conector->select($sql);
        $arr=array();
        for ($i=0; $i <count($datos) ; $i++) {
            $arr[]=array(
                'MunCod'  =>  utf8_encode(trim($datos[$i][0])),
                'MunNom'	 =>  utf8_encode($datos[$i][1])
            );
        }
        $retorno=json_encode(array("status_code"=>"200","data"=>$arr));
        return $retorno;
    }


}
