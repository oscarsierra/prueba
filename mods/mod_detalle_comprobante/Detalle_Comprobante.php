<?php
require_once("../../conf/Configuracion.php");
require_once("../../jumichica/ConectorMSSQL.php");
require_once("../../mods/mod_usuarios/Usuario.php");
/**
 *
 */
class Detalle_Comprobante {

	private $conector;
    private $moduser;
    function __construct() {
        $this->moduser=new Usuario();
        $this->conector = new ConectorMSSQL(Variables::$HOST_BD, Variables::$NOMBRE_BD, Variables::$USUARIO_BD, Variables::$CLAVE_BD);
    }

    public function crear($api,$usuario,$clave,$ComNum,$EmpresaId,$EmpresaNit,$TipComId,$ComCons,$PlaCueCod,$ComTipMov,$ComVal,$ComNitDet,$ComValRet,$ComConDet,$CenCosId,$ComDocSop,$rest=FALSE)
    {
    	if($this->moduser->validarUser($usuario, $clave, $EmpresaId, $EmpresaNit)){
        	$sql = "INSERT INTO COMPROBANTEDETALLE(ComNum , EmpresaId , TipComId , ComCons
         	, PlaCueCod , ComTipMov , ComVal , ComNitDet , ComValRet, ComConDet , CenCosId , ComDocSop )
        	 VALUES('$ComNum' , '$EmpresaId' , '$TipComId' , '$ComCons' 
         	, '$PlaCueCod' , '$ComTipMov' , '$ComVal' , '$ComNitDet' , '$ComValRet', '$ComConDet' , '$CenCosId' , '$ComDocSop' )";

            if($rest==TRUE){
                 $dat=$this->conector->transformarRest($dat);
                 echo $dat;
            }else{
                $dat=$this->conector->transformar($dat);
                return new soapval('return', 'xsd:string', $dat);
            }
		 }else{
            if($rest==TRUE){
               echo json_encode(array("error"=>"ERROR: ","respuestas"=>"Invalido algunos de estos datos: usuario, clave, empresaid o empresanit"));
            }else{
                return new soapval('return', 'xsd:string', 'Invalido algunos de estos datos: usuario, clave, empresaid o empresanit');
            }
        }
    }
    public function modificar($api , $usuario , $clave , $ComNum , $EmpresaId , $EmpresaNit , $TipComId , $ComCons
         , $PlaCueCod , $ComTipMov , $ComVal , $ComNitDet , $ComValRet, $ComConDet , $CenCosId , $ComDocSop ,$rest=FALSE)
    {
    	if($this->moduser->validarUser($usuario, $clave, $EmpresaId, $EmpresaNit)){
 			$sql = "UPDATE COMPROBANTEDETALLE SET
        		TipComId='$TipComId' , ComCons='$ComCons', PlaCueCod='$PlaCueCod' , ComTipMov='$ComTipMov' ,
        		ComVal='$ComVal' , ComNitDet='$ComNitDet' , ComValRet='$ComValRet', ComConDet='$ComConDet' , CenCosId='$CenCosId' ,
        		ComDocSop='$ComDocSop' WHERE  ComNum='$ComNum' AND EmpresaId='$EmpresaId' AND ComCons='$ComCons' AND TipComId='$TipComId'";
         		$dat=$this->conector->insert($sql);
                if($rest==TRUE){
                     $dat=$this->conector->transformarRest($dat);
                     echo $dat;
                }else{
                    $dat=$this->conector->transformar($dat);
                    return new soapval('return', 'xsd:string', $dat);
                }
		 }else{
    		 return new soapval('return', 'xsd:string', 'Invalido algunos de estos datos: usuario, clave, empresaid o empresanit');
    	}
    }
    public function consultar($api , $usuario , $clave , $ComNum , $EmpresaId , $EmpresaNit ,$rest=FALSE )
    {
    	if($this->moduser->validarUser($usuario, $clave, $EmpresaId, $EmpresaNit)){
        	$sql = "SELECT ComNum , EmpresaId ,  TipComId , ComCons
         		, PlaCueCod , ComTipMov , ComVal , ComNitDet , ComValRet, ComConDet , CenCosId , ComDocSop
        		 FROM COMPROBANTEDETALLE WHERE ComNum like '%$ComNum%' AND EmpresaId= '$EmpresaId'";
         		$datos=$this->conector->select($sql);
                if($rest==TRUE){
                     $datos=$this->conector->transformarRest($datos);
                     echo $datos;
                }else{
                    $datos=$this->conector->transformar($datos);
                    return new soapval('return', 'xsd:string', $datos);
                }
         }else{
            if($rest==TRUE){
               echo json_encode(array("error"=>"ERROR: ","respuestas"=>"Invalido algunos de estos datos: usuario, clave, empresaid o empresanit"));
            }else{
                return new soapval('return', 'xsd:string', 'Invalido algunos de estos datos: usuario, clave, empresaid o empresanit');
            }
        }
    }
}


?>
