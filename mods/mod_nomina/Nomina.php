<?php
require_once("../../conf/Configuracion.php");
require_once("../../jumichica/ConectorMSSQL.php");
require_once("../../mods/mod_usuarios/Usuario.php");

/**
 *
 */
class Nomina{
	private $moduser;
	private $conector;
    function __construct() {
    	$this->moduser=new Usuario();
      $this->conector = new ConectorMSSQL(Variables::$HOST_BD2, Variables::$NOMBRE_BD2, Variables::$USUARIO_BD2, Variables::$CLAVE_BD2);
    }
public function get_nomina($empresaid){
		$sql = "SELECT e.EmpleNumDoc, cc.ContConceptoID, cc.ContConcepValor, cc.ContConcepDet,
    e.EmpIde,con.ContID,cc.ContConcepID,cc.ContConcepPorc,cc.ContConcepFchCrea,cc.ContConcepUsuCod,cc.ContConcepVigen,
    cc.ContConcepDet,cc.ContTerceroID,cc.ContConcepAB,cc.ContConcepNC
    FROM Empleados AS e, ContratosConceptos AS cc, Contratos AS con
    WHERE e.EmpleID=con.ContEmpleID AND con.ContID=cc.ContID AND e.EmpIde=cc.EmpIde AND cc.EmpIde=con.EmpIde AND cc.EmpIde='$empresaid'";
		$datos=$this->conector->select($sql);
		$arr=array();
		for ($i=0; $i <count($datos) ; $i++) {
			$arr[]=array(
				'EmpleNumDoc' =>trim($datos[$i][0]),
				'ContConceptoID' =>trim($datos[$i][1]),
				'ContConcepValor' =>trim($datos[$i][2]),
				'ContConcepDet' =>utf8_encode(trim($datos[$i][3])),
				'EmpIde' =>trim($datos[$i][4]),
				'ContID' =>trim($datos[$i][5]),
				'ContConcepID' =>trim($datos[$i][6]),
				'ContConcepPorc' =>trim($datos[$i][7]),
				'ContConcepFchCrea' =>trim($datos[$i][8]),
				'ContConcepUsuCod' =>trim($datos[$i][9]),
				'ContConcepVigen' =>trim($datos[$i][10]),
				'ContConcepDet' =>trim($datos[$i][11]),
				'ContTerceroID' =>trim($datos[$i][12]),
				'ContConcepAB' =>trim($datos[$i][13]),
				'ContConcepNC' =>trim($datos[$i][14])
			);
		}
		return $arr;
  }
  public function set_nomina($EmpIde,$ContID,$ContConcepID,$ContConceptoID,$ContConcepValor,
	$ContConcepPorc,$ContConcepFchCrea,$ContConcepUsuCod,$ContConcepVigen,$ContConcepDe,$ContTerceroID,$ContConcepAB,$ContConcepNC){
	  		$sql ="INSERT INTO ContratosConceptos(EmpIde,ContID,ContConcepID,ContConceptoID,ContConcepValor,ContConcepPorc,
				ContConcepFchCrea,ContConcepUsuCod,ContConcepVigen,ContConcepDet,ContTerceroID,ContConcepAB,ContConcepNC)
				VALUES($EmpIde,$ContID,$ContConcepID,$ContConceptoID,$ContConcepValor,$ContConcepPorc,
				'$ContConcepFchCrea',$ContConcepUsuCod,'$ContConcepVigen','$ContConcepDe',$ContTerceroID,$ContConcepAB,$ContConcepNC)";
				//return  array('sql' => $sql );
				if($this->conector->insert($sql))
					return array("ok"=>"Los datos e guardaron correctamente");
				else
					return array("error"=>"Error al guardar los datos");

	   }

		 public function update_nomina($EmpIde,$ContConcepID,$ContConceptoID,$ContConcepValor,$ContConcepPorc,
	 	 $ContConcepFchCrea,$ContConcepUsuCod,$ContConcepVigen,$ContConcepDe,$ContTerceroID,$ContConcepAB,$ContConcepNC,$UsuarioID){
					$sql="SELECT ContID FROM Contratos WHERE ContEmpleID=".$UsuarioID;
					$datos=$this->conector->select($sql);
	 	  		$sql ="UPDATE  ContratosConceptos  SET EmpIde='$EmpIde',ContConcepID='$ContConcepID',ContConceptoID='$ContConceptoID',ContConcepValor='$ContConcepValor',ContConcepPorc='$ContConcepPorc',
	 				ContConcepFchCrea='$ContConcepFchCrea',ContConcepUsuCod='$ContConcepUsuCod',ContConcepVigen='$ContConcepVigen',ContConcepDet='$ContConcepDe',ContTerceroID='$ContTerceroID',
					ContConcepAB='$ContConcepAB',ContConcepNC='$ContConcepNC' WHERE ContID=".$datos[0][0];
	 				if($this->conector->update($sql))
	 				return array("ok"=>"Los datos se actualizaron correctamente");
	 				else
	 				return array("error"=>"Error al actualizar los datos");

	 	   }
    }
?>
