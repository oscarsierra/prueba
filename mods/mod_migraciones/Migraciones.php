<?php
require_once("../../conf/Configuracion.php");
require_once("../../jumichica/ConectorMSSQL.php");
require_once("../../mods/mod_contabilizacion/Contabilizacion.php");

/**
 *
 */
class Migraciones {
	private $conector;
	private $conector2;
    function __construct() {
        $this->conector = new ConectorMSSQL(Variables::$HOST_BD, Variables::$NOMBRE_BD, Variables::$USUARIO_BD, Variables::$CLAVE_BD);
          $this->conector2 = new ConectorMSSQL(Variables::$HOST_BD2, Variables::$NOMBRE_BD2, Variables::$USUARIO_BD2, Variables::$CLAVE_BD2);
    }

    public function migrar_clientes($id_empresa){
			$mod_conta = new Contabilizacion();
			$metodoR=$mod_conta->migrar_clientes($id_empresa);
			$RegistrarClientes=$mod_conta->RegistrarClientes(json_encode($metodoR));
			$respuesta=array("status"=>"200",'data'=>$metodoR,'dataXeo'=>json_decode($RegistrarClientes));
			return  json_encode($respuesta);
    }

    public function migrar_facturas_credito($id_empresa){
		$mod_conta = new Contabilizacion();
		$migrar_facturas_credito=$mod_conta->migrar_facturas_credito($id_empresa);
		$mod_fact = new Factura_credito();
		$RegistrarFacturasCredito=array();
		$dataSinco=array();

		for ($i=0; $i <count($migrar_facturas_credito) ; $i++) {
			$get_factura_credito=$mod_fact->get_factura_creditoCambio(	$migrar_facturas_credito[$i][0],
																		$migrar_facturas_credito[$i][1]);
			$arr=array();
			for ($j=0; $j < count($get_factura_credito); $j++) { 
				if($migrar_facturas_credito[$i][3]!=1 ){ $indEvento="C";}else{$indEvento=$get_factura_credito[$j]['IndEvento'];}

					$arr[]=array(
					'codigo' =>  trim($get_factura_credito[$j]['CedONit']),
					'valor' =>  $get_factura_credito[$j]['Valor'],
					'numCuotas' =>  $get_factura_credito[$j]['NCuotas'],
					'codDoc' =>  $get_factura_credito[$j]['CodDoc'],
					'numDoc' =>  $get_factura_credito[$j]['Numero'],
					'tipoTrans' =>  $get_factura_credito[$j]['TipoTran'],
					'diasAmort' =>  $get_factura_credito[$j]['DiasA'],
					'fechaOpe' =>  $get_factura_credito[$j]['FechaOpe'],
					'sucursal' =>  $get_factura_credito[$j]['Sucursal'],
					'indEvento' => $indEvento 
					);
				
			}
			$dataSinco[]=$arr;
			$RegistrarFacturasCredito[]=json_decode($mod_conta->RegistrarFacturasCredito(json_encode($arr)));
		}
				
		$respuesta=array( "status"=>"200",'data'=>$dataSinco,
							'dataXeo'=>$RegistrarFacturasCredito);
							
		return json_encode($respuesta); 
    }


    public function migrar_contabilizacion2($id_empresa){
			$mod_conta = new Contabilizacion();
			$datos=$mod_conta->get_contabilizacion2($id_empresa);
			$RegistrarContabilizacion='[]';
			$metodoR=array();
			$arr=array();
				for ($i=0; $i < count($datos) ; $i++) {
					$Comnum=$datos[$i]['ComNum'];
					$TipComId=$datos[$i]['TipComId'];
					$metodoR=$mod_conta->migrar_contabilizacion($id_empresa,$Comnum,$TipComId);
					$RegistrarContabilizacion=$mod_conta->RegistrarContabilizacion(json_encode($metodoR));
					$RegistrarContabilizacion=json_decode($RegistrarContabilizacion);
					
					if(isset($RegistrarContabilizacion->codigo) && $RegistrarContabilizacion->codigo=="200"){
							$mod_conta->update_comprobante($id_empresa,$Comnum,$TipComId);
					}
		
					$arr[]=array('data'=>$metodoR,'dataXeo'=>$RegistrarContabilizacion);
				}
			$respuesta=array("status"=>"200","data"=>$arr);
			return json_encode($respuesta);
			}

    public function migrarDxN($id_empresa){
	    $mod_conta = new Contabilizacion();
	    $datos=$mod_conta->PeriodoLiqCob($id_empresa);
			$respuesta=array();
			$array=array();
			
	    for ($i=0; $i < count($datos); $i++) { 
	      $json_encode=$mod_conta->CobroDxN($datos[$i][0],$datos[$i][1]);
				$jsonArray=json_decode($json_encode);
				
	      if(isset($jsonArray->codigo) && $jsonArray->codigo=="200" ){
					$data=$jsonArray->data;

	        for ($j=0; $j <count($data) ; $j++) {
						$valid=$mod_conta->migrarDxN($data[$j],$id_empresa);
	          if($valid==true){
	            $status="Migracion Exitosa";
	            $mod_conta->actualizarPeriodoCambio($id_empresa,$datos[$i][2]);
	          }else{
	           $status="Migracion documento no tiene contrato";
	          }
	          array_push($array,array("status"=>$status,"dataXeo"=>$data[$j]));
					}

	      }else{

	      	if(isset($jsonArray->msg)){
	       	 array_push($array,array("status"=>$jsonArray->msg));
	      	}else{
	       	 array_push($array,array("status"=>"Error de webservice XEO "));
					}
					
				}
				//array_push($array,array("data"=>$json_decode,"dataXeo"=>$respuestaXEOArr));

	      $respuesta[]=array("status"=>200,"msg"=>"","PerLiq"=>$datos[$i][0],"PerCorte"=>$datos[$i][1],"data"=>$array);
			}

			if(empty($respuesta)){
				$respuesta[]=array("status"=>200,"msg"=>"No existen periodos a liquidar");

			}

			//$respuesta[]=array("status"=>"200","data"=>$array);

	   return json_encode($respuesta);
	}   

    public function notificarCxN($id_empresa){
	    $mod_conta = new Contabilizacion();
	    $datos=$mod_conta->PeriodoLiqNot($id_empresa);
			$respuesta=array();
			$array=array();
	    for ($i=0; $i < count($datos); $i++) { 
	      $json_encode=$mod_conta->notificarCxN($datos[$i][0],$datos[$i][1],$id_empresa);
				$json_decode=json_decode($json_encode);

	      if($json_decode->status!="200"){
					$respuestaXEOArr=array("status"=>$json_decode->status);
	      }else{
	        $respuestaXEO=$mod_conta->RegistrarPagosDxN(json_encode($json_decode->data));
	        $respuestaXEOArr=array("status"=>"200","data"=>json_decode($respuestaXEO));
	        //$respuesta[]=array("status"=>$respuestaXEOArr->msg);
	        $mod_conta->actualizarPeriodoCamb($id_empresa,$datos[$i][0]);
				}
				
				array_push($array,array("data"=>$json_decode,"dataXeo"=>$respuestaXEOArr));
			}
			$respuesta = array("status"=>"200","data"=>$array);
			
	    return json_encode($respuesta);
	}
}
