<?php
require_once("../../conf/Configuracion.php");
require_once("../../jumichica/ConectorMSSQL.php");
require_once("../../mods/mod_usuarios/Usuario.php");
/**
 * 
 */
class Centro_Costo {
	
	private $conector;
	private $moduser;
    function __construct() {
    	$this->moduser=new Usuario();
        $this->conector = new ConectorMSSQL(Variables::$HOST_BD, Variables::$NOMBRE_BD, Variables::$USUARIO_BD, Variables::$CLAVE_BD);
    }
    public function crear($api, $usuario, $clave, $EmpresaId,$EmpresaNit,$CenCosId, $CenCosNom,$rest=FALSE){
    	if($this->moduser->validarUser($usuario, $clave, $EmpresaId, $EmpresaNit)){
    		$sql = "INSERT INTO CENTROSCOSTO(EmpresaId,CenCosId, CenCosNom) VALUES('$EmpresaId','$CenCosId', '$CenCosNom')";
	        $re=$this->conector->insert($sql);
            if($rest==TRUE){
                 echo $re;
            }else{
                return new soapval('return', 'xsd:string', $re);
            }
    	}else{
    		if($rest==TRUE){
                echo json_encode(array("error"=>"ERROR: ","respuestas"=>"Invalido algunos de estos datos: usuario, clave, empresaid o empresanit"));
            }else{
                 return new soapval('return', 'xsd:string', 'Invalido algunos de estos datos: usuario, clave, empresaid o empresanit');
            }
    	}
        
    }
    
    public function modificar($api, $usuario, $clave, $EmpresaId,$EmpresaNit,$CenCosId, $CenCosNom,$rest=FALSE){
        if($this->moduser->validarUser($usuario, $clave, $EmpresaId, $EmpresaNit)){
            $sql = "UPDATE CENTROSCOSTO SET CenCosNom='$CenCosNom' 
            WHERE EmpresaId='$EmpresaId' AND CenCosId='$CenCosId'";
            $re=$this->conector->insert($sql);
            if($rest==TRUE){
                 echo $re;
            }else{
                return new soapval('return', 'xsd:string', $re);
            }
        }else{
            if($rest==TRUE){
                echo json_encode(array("error"=>"ERROR: ","respuestas"=>"Invalido algunos de estos datos: usuario, clave, empresaid o empresanit"));
            }else{
                 return new soapval('return', 'xsd:string', 'Invalido algunos de estos datos: usuario, clave, empresaid o empresanit');
            }
        }
    }
    public function consultar($api, $usuario, $clave, $EmpresaId,$EmpresaNit,$CenCosId, $CenCosNom,$rest=FALSE){
        if($this->moduser->validarUser($usuario, $clave, $EmpresaId, $EmpresaNit)){
            $sql = "SELECT EmpresaId, CenCosId, CenCosNom FROM CENTROSCOSTO 
            WHERE EmpresaId='$EmpresaId' AND CenCosId='$CenCosId'";
            $datos=$this->conector->select($sql);
            if($rest==TRUE){
                 $datos=$this->conector->transformarRest($datos);
                 echo $datos;
            }else{
                $datos=$this->conector->transformar($datos);
                return new soapval('return', 'xsd:string', $datos);
            }
        }else{
            if($rest==TRUE){
                echo json_encode(array("error"=>"ERROR: ","respuestas"=>"Invalido algunos de estos datos: usuario, clave, empresaid o empresanit"));
            }else{
                return new soapval('return', 'xsd:string', 'Invalido algunos de estos datos: usuario, clave, empresaid o empresanit');
            }
        }
    }
}

?>