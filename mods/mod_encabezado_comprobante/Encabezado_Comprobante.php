<?php
require_once("../../conf/Configuracion.php");
require_once("../../jumichica/ConectorMSSQL.php");
require_once("../../mods/mod_usuarios/Usuario.php");
/**
 *
 */
class Encabezado_Comprobante {
	private $conector;
	private $moduser;
	function __construct() {
		$this->moduser=new Usuario();
		$this->conector = new ConectorMSSQL(Variables::$HOST_BD, Variables::$NOMBRE_BD, Variables::$USUARIO_BD, Variables::$CLAVE_BD);
	}

    public function crear($api , $usuario , $clave , $ComNum , $EmpresaId , $EmpresaNit,$TipComId, $ComFec,
         $ComNit ,$ComCon ,$ComPerAgno , $ComPerMes, $ComUltCons, $ComAnu,$rest=FALSE)
    {
       if($this->moduser->validarUser($usuario, $clave, $EmpresaId, $EmpresaNit)){
            $sql="INSERT INTO COMPROBANTE (ComNum , EmpresaId , TipComId, ComFec,
             ComNit , ComCon , ComPerAgno ,  ComPerMes, ComUltCons, ComAnu) VALUES ('$ComNum' , '$EmpresaId' , '$TipComId', '$ComFec',
             '$ComNit' ,'$ComCon' ,'$ComPerAgno' , '$ComPerMes', $ComUltCons, $ComAnu)";

            $dat=$this->conector->insert($sql);
            if($rest==TRUE){
                 $dat=$this->conector->transformarRest($dat);
                 echo $dat;
            }else{
                $dat=$this->conector->transformar($dat);
                return new soapval('return', 'xsd:string', $dat);
            }
		}else{
             if($rest==TRUE){
                echo json_encode(array("error"=>"ERROR: ","respuestas"=>"Invalido algunos de estos datos: usuario, clave, empresaid o empresanit"));
             }else{
                 return new soapval('return', 'xsd:string', 'Invalido algunos de estos datos: usuario, clave, empresaid o empresanit');
             }
    	}

    }

    public function modificar($api , $usuario , $clave , $ComNum , $EmpresaId , $EmpresaNit,$TipComId, $ComFec,
         $ComNit ,$ComCon ,$ComPerAgno , $ComPerMes,$rest=FALSE)
    {
      	if($this->moduser->validarUser($usuario, $clave, $EmpresaId, $EmpresaNit)){
        	$sql="UPDATE COMPROBANTE SET
         	TipComId='$TipComId', ComFec='$ComFec', ComNit='$ComNit' , ComCon='$ComCon' , ComPerAgno='$ComPerAgno' ,
         	ComPerMes='$ComPerMes' WHERE ComNum='$ComNum' AND EmpresaId='$EmpresaId' AND TipComId='$TipComId' ";

         	$dat=$this->conector->insert($sql);
            if($rest==TRUE){
                 $dat=$this->conector->transformarRest($dat);
                 echo $dat;
            }else{
                $dat=$this->conector->transformar($dat);
                return new soapval('return', 'xsd:string', $dat);
            }
      	}else{
             if($rest==TRUE){
                echo json_encode(array("error"=>"ERROR: ","respuestas"=>"Invalido algunos de estos datos: usuario, clave, empresaid o empresanit"));
             }else{
                 return new soapval('return', 'xsd:string', 'Invalido algunos de estos datos: usuario, clave, empresaid o empresanit');
             }
        }
    }

    public function consultar($api , $usuario , $clave , $ComNum , $EmpresaId , $EmpresaNit,$rest=FALSE)
    {
         //aca va la validacion de empresa id con empresa nit
        if($this->moduser->validarUser($usuario, $clave, $EmpresaId, $EmpresaNit)){
         	$sql="SELECT ComNum , EmpresaId , TipComId, ComFec,
         	ComNit , ComCon , ComPerAgno ,  ComPerMes FROM COMPROBANTE
         	WHERE ComNum like '%$ComNum%' AND EmpresaId= '$EmpresaId' ";

        	$datos=$this->conector->select($sql);
         	if($rest==TRUE){
                 $datos=$this->conector->transformarRest($datos);
                 echo $datos;
            }else{
                $datos=$this->conector->transformar($datos);
                return new soapval('return', 'xsd:string', $datos);
            }
		}else{
			if($rest==TRUE){
				echo json_encode(array("error"=>"ERROR: ","respuestas"=>"Invalido algunos de estos datos: usuario, clave, empresaid o empresanit"));
			}else{
				return new soapval('return', 'xsd:string', 'Invalido algunos de estos datos: usuario, clave, empresaid o empresanit');
			}
       	}
    }

}


?>
