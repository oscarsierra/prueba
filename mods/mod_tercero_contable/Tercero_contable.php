<?php
require_once("../../conf/Configuracion.php");
require_once("../../jumichica/ConectorMSSQL.php");
require_once("../../mods/mod_usuarios/Usuario.php");

/**
 *
 */
class Tercero_contable{
	private $moduser;
	private $conector;
    function __construct() {
    	$this->moduser=new Usuario();
      $this->conector = new ConectorMSSQL(Variables::$HOST_BD, Variables::$NOMBRE_BD, Variables::$USUARIO_BD, Variables::$CLAVE_BD);
    }
public function get_tercero_contable($empresaid){
		$sql = "SELECT DirNitId,DirNitRazSoc,DirNitDir,DirNitTel,(CONVERT (date, SYSDATETIME())),DirNitFax,DirNitEmail,DirNitCon,'*',CASE
    WHEN DirNitPro=1 THEN 'P'
	  WHEN DirNitCli=1 THEN 'C'
    END AS dirtipo,TipDocCod,CASE
    WHEN TipDocCod='13' THEN 'CC'
    END AS nomtipdoc,DirNitNom1,DirNitNom2,DirNitApe1,DirNitApe2,DepCod,MunCod  FROM DIRECTORIONITS WHERE EmpresaId='$empresaid'";
		$datos=$this->conector->select($sql);
		$arr=array();
		for ($i=0; $i <count($datos) ; $i++) {
			$arr[]=array(
				'Codigo' => utf8_encode(trim($datos[$i][0])),
				'Nit' => utf8_encode(trim($datos[$i][0])),
				'RazonSocial' => utf8_encode($datos[$i][1]),
				'Direccion' => utf8_encode($datos[$i][2]),
				'Telefono' => utf8_encode($datos[$i][3]),
				'FechaC' => utf8_encode($datos[$i][4]),
				'Fax' => utf8_encode($datos[$i][5]),
				'Email' => utf8_encode($datos[$i][6]),
				'Observaciones' => utf8_encode(trim($datos[$i][7])),
				'Ciudad' => utf8_encode($datos[$i][8]),
				'Clase' => utf8_encode($datos[$i][9]),
				'TipoId' => utf8_encode($datos[$i][11]),
				'PrimerNom' => utf8_encode(trim($datos[$i][12])),
				'SegundoNom' => utf8_encode(trim($datos[$i][13])),
				'PrimerApel' => utf8_encode(trim($datos[$i][14])),
				'SegundoApel' => utf8_encode(trim($datos[$i][15])),
				'CodDep' => utf8_encode($datos[$i][16]),
				'CodCiudad' => utf8_encode($datos[$i][17])
			);
		}
		return $arr;

  }
}

?>
