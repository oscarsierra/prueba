<?php
require_once("../../conf/Configuracion.php");
require_once("../../jumichica/ConectorMSSQL.php");
require_once("../../mods/mod_usuarios/Usuario.php");

/**
 *
 */
class Factura_credito {
	private $moduser;
	private $conector;
    function __construct() {
    	$this->moduser=new Usuario();
        $this->conector = new ConectorMSSQL(Variables::$HOST_BD, Variables::$NOMBRE_BD, Variables::$USUARIO_BD, Variables::$CLAVE_BD);
    }
	public function get_factura_credito($empresaid,$FacNun,$nit=""){
		$sql_nit="";
		if($nit!="")
			$sql_nit="AND f.DirNitId='".$nit."' ";
			$sql = "SELECT f.DirNitId, cu.carval , '1', f.TIPCOMID, f.FACNUM,
					CASE  WHEN drn.DirNitSoc=1 AND drn.DirNitEmp=1 
					THEN 'PAS' WHEN drn.DirNitSoc=1 AND drn.DirNitEmp<>1 
					THEN 'PAS' WHEN drn.DirNitSoc<>1 AND drn.DirNitEmp=1 
					THEN 'PEM' ELSE 'PNA' END as tipotran,
					'30',
					CONVERT(char(10), f.FACFEC, 126),
					CASE WHEN f.FACCENCOS ='000' OR f.FACCENCOS =''  THEN '111' ELSE f.FACCENCOS END AS Sucursal,
					CASE WHEN f.FacAnu = 1 THEN 'A' ELSE 'M' END AS facanu,
					CASE  WHEN f.FacImp =1 THEN 'A' ELSE '' END AS facimp 
					FROM FACTURA as f,
					DIRECTORIONITS as drn , cuenta as cu
					WHERE drn.DirNitId=f.DirNitId AND f.FacNumCom='' AND f.FACNUM='$FacNun'
					AND  f.ForPagCod in (SELECT fp.ForPagCod from FORMASPAGO as fp WHERE fp.ForPagIndCre=1) 
									
					AND f.FACNUM = cu.CarNum
					AND f.empresaid=cu.empresaid
					AND f.tipcomid=cu.tipcomid
					
					AND f.EmpresaId='$empresaid' $sql_nit";
			$datos=$this->conector->select($sql);
			$arr=array();
			for ($i=0; $i <count($datos) ; $i++) {
				$IndEvento=$datos[$i][9];
				$arr[]=array(
					'CedONit' =>  trim($datos[$i][0]),
					'Valor' =>  $datos[$i][1],
					'NCuotas' =>  $datos[$i][2],
					'CodDoc' =>  $datos[$i][3],
					'Numero' =>  $datos[$i][3].$datos[$i][4],
					'TipoTran' =>  $datos[$i][5],
					'DiasA' =>  $datos[$i][6],
					'FechaOpe' =>  $datos[$i][7],
					'Sucursal' =>  Variables::$SUCURSAL_FACTURACION,
					'IndEvento' =>  $IndEvento
				);
			}
			
			return $arr;
 }

 public function get_factura_creditoCambio($empresaid,$FacNun,$nit=""){
	$sql_nit="";
	if($nit!="")
		$sql_nit="AND f.DirNitId='".$nit."' ";
		$sql = "SELECT f.DirNitId, cu.carval , '1', f.TIPCOMID, f.FACNUM,
				CASE  WHEN drn.DirNitSoc=1 AND drn.DirNitEmp=1 
				THEN 'PAS' WHEN drn.DirNitSoc=1 AND drn.DirNitEmp<>1 
				THEN 'PAS' WHEN drn.DirNitSoc<>1 AND drn.DirNitEmp=1 
				THEN 'PEM' ELSE 'PNA' END as tipotran,
				'30',
				CONVERT(char(10), f.FACFEC, 126),
				CASE WHEN f.FACCENCOS ='000' OR f.FACCENCOS =''  THEN '111' ELSE f.FACCENCOS END AS Sucursal,
				CASE WHEN f.FacAnu = 1 THEN 'A' ELSE 'M' END AS facanu,
				CASE  WHEN f.FacImp =1 THEN 'A' ELSE '' END AS facimp 
				FROM FACTURA as f,DIRECTORIONITS as drn , COMPROBANTE co, cuenta cu
				WHERE drn.DirNitId=f.DirNitId AND f.FacNumCom='' AND f.FACNUM='$FacNun'
				AND  f.ForPagCod in (SELECT fp.ForPagCod from FORMASPAGO as fp WHERE fp.ForPagIndCre=1) 
				AND f.EmpresaId='$empresaid' $sql_nit AND co.TipComId = f.TipComId AND CO.ComNum=F.FacNum
				AND co.ComNum = cu.CarNum
				AND co.empresaid=cu.empresaid
				AND co.tipcomid=cu.tipcomid
				AND ( co.ComCambio is null or co.ComCambio !=0 ) ";
		$datos=$this->conector->select($sql);
		$arr=array();
		for ($i=0; $i <count($datos) ; $i++) {
			$IndEvento=$datos[$i][9];
			$arr[]=array(
				'CedONit' =>  trim($datos[$i][0]),
				'Valor' =>  $datos[$i][1],
				'NCuotas' =>  $datos[$i][2],
				'CodDoc' =>  $datos[$i][3],
				'Numero' =>  $datos[$i][3].$datos[$i][4],
				'TipoTran' =>  $datos[$i][5],
				'DiasA' =>  $datos[$i][6],
				'FechaOpe' =>  $datos[$i][7],
				'Sucursal' =>  Variables::$SUCURSAL_FACTURACION,
				'IndEvento' =>  $IndEvento
			);
		}
		
		return $arr;
}


 public function get_factura_credito_fecha($empresaid,$year,$month,$nit=""){
	 $sql_nit="";
	 if($nit!="")
		 $sql_nit="AND f.DirNitId='".$nit."' ";
	 	if(strlen($month)==1)
		 $month="0".$month;
		 $sql = "	SELECT f.DirNitId,cu.carval,'1',f.TIPCOMID,f.FACNUM,
		 			CASE  
			 			WHEN drn.DirNitSoc=1 AND drn.DirNitEmp=1 
						THEN 'PAS' 
						WHEN drn.DirNitSoc=1 AND drn.DirNitEmp<>1 
						THEN 'PAS' 
						WHEN drn.DirNitSoc<>1 AND drn.DirNitEmp=1 
						THEN 'PEM' ELSE 'PNA' END as tipotran,
					'30',
					CONVERT(char(10), f.FACFEC, 126),
					CASE 
						WHEN f.FACCENCOS ='000' OR f.FACCENCOS =''  
						THEN '111' ELSE f.FACCENCOS END AS Sucursal,
					CASE 
						WHEN f.FacAnu =1 
						THEN 'A' ELSE 'C' END AS facanu,
					CASE  
						WHEN f.FacImp =1 
						THEN 'C' ELSE '' END AS facimp
		 			FROM FACTURA as f,DIRECTORIONITS as drn  ,cuenta as cu
					WHERE drn.DirNitId=f.DirNitId AND YEAR(f.FACFEC)='$year' 
					AND f.FACNUM = cu.CarNum
					AND f.empresaid=cu.empresaid
					AND f.tipcomid=cu.tipcomid
					AND MONTH(f.FACFEC)='$month' AND f.FacNumCom=''  
					AND  f.ForPagCod in (SELECT fp.ForPagCod from FORMASPAGO as fp 
					WHERE fp.ForPagIndCre=1) AND f.EmpresaId='$empresaid' $sql_nit";
		 $datos=$this->conector->select($sql);
		 $arr=array();
		 for ($i=0; $i <count($datos) ; $i++) {
			$IndEvento=$datos[$i][9];
			 $arr[]=array(
				 'CedONit' =>  trim($datos[$i][0]),
				 'Valor' =>  $datos[$i][1],
				 'NCuotas' =>  $datos[$i][2],
				 'CodDoc' =>  $datos[$i][3],
				 'Numero' =>  $datos[$i][3].$datos[$i][4],
				 'TipoTran' =>  $datos[$i][5],
				 'DiasA' =>  $datos[$i][6],
				 'FechaOpe' =>  $datos[$i][7],
				 'Sucursal' =>  Variables::$SUCURSAL_FACTURACION,
				 'IndEvento' =>  $IndEvento
			 );
		 }
		 return $arr;
}
}

?>
