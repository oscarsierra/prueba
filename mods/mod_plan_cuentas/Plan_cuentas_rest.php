<?php
require_once("../../conf/Configuracion.php");
require_once("../../jumichica/ConectorMSSQL.php");
require_once("../../mods/mod_usuarios/Usuario.php");

/**
 *
 */
class Plan_cuentas_rest {
	private $moduser;
	private $conector;
    function __construct() {
    	$this->moduser=new Usuario();
        $this->conector = new ConectorMSSQL(Variables::$HOST_BD, Variables::$NOMBRE_BD, Variables::$USUARIO_BD, Variables::$CLAVE_BD);
    }
    public function get_plan_cuentas($empresaid){
				$sql = "SELECT PlaCueCod,PlaCueNom,PlaCueDep,PlaCueIndRecMov,PlaCueIndRet,PlaCuePorRet,PlaCueIndDocSop,PlaCueIndReiAnu,PlaCueIndEst,PlacueNiif FROM PLANCUENTA WHERE EmpresaId='$empresaid'";
				$datos=$this->conector->select($sql);
				$arr=array();
				for ($i=0; $i <count($datos) ; $i++) {
					$arr[]=array(
						'PlaCueCod' => trim(utf8_encode($datos[$i][0])),
						'PlaCueNom' => trim(utf8_encode($datos[$i][1])),
						'PlaCueDep' => trim(utf8_encode($datos[$i][2])),
						'PlaCueIndRecMov' => trim(utf8_encode($datos[$i][3])),
						'PlaCueIndRet' => trim(utf8_encode($datos[$i][4])),
						'PlaCuePorRet' => trim(utf8_encode($datos[$i][5])),
						'PlaCueIndDocSop' => trim(utf8_encode($datos[$i][6])),
						'PlaCueIndReiAnu' => trim(utf8_encode($datos[$i][7])),
						'PlaCueIndEst' => trim(utf8_encode($datos[$i][8])),
						'PlacueNiif' => trim(utf8_encode($datos[$i][9]))
					 );
				}
				return $arr;
  }
  public function set_plan_cuentas($EmpresaIdN,$PlaCueCod,$PlaCueNom,$PlaCueDep,$PlaCueIndRecMov,$PlaCueIndRet,$PlaCuePorRet,$PlaCueIndDocSop,$PlaCueIndReiAnu,$PlaCueIndEst,$PlacueNiif){
          $sql ="INSERT INTO PLANCUENTA (EmpresaId,PlaCueCod,PlaCueNom,PlaCueDep,PlaCueIndRecMov,PlaCueIndRet,PlaCuePorRet,PlaCueIndDocSop,PlaCueIndReiAnu,PlaCueIndEst,PlaCueChe,PlacueNiif)
          VALUES ('$EmpresaIdN','$PlaCueCod','$PlaCueNom','$PlaCueDep','$PlaCueIndRecMov','$PlaCueIndRet','$PlaCuePorRet','$PlaCueIndDocSop','$PlaCueIndReiAnu','$PlaCueIndEst','0','$PlacueNiif')";
          //echo "dd".$sql;
					if($this->conector->insert($sql))
						return array('ok' => "Datos guardados correctamente");
					else
						return array('error' => "-Error al guardar los datos");
}
public function update_plan_cuentas($PlaCueCod,$PlaCueNom,$PlaCueDep,$PlaCueIndRecMov,$PlaCueIndRet,$PlaCuePorRet,$PlaCueIndDocSop,$PlaCueIndReiAnu,$PlaCueIndEst,$PlacueNiif){
    $sql ="UPDATE PLANCUENTA SET PlaCueNom='$PlaCueNom',PlaCueDep='$PlaCueDep',PlaCueIndRecMov='$PlaCueIndRecMov',PlaCueIndRet='$PlaCueIndRet',PlaCuePorRet='$PlaCuePorRet',PlaCueIndDocSop='$PlaCueIndDocSop',PlaCueIndReiAnu='$PlaCueIndReiAnu',PlaCueIndEst='$PlaCueIndEst',PlacueNiif='$PlacueNiif'
     WHERE PlaCueCod='$PlaCueCod'";
    if($this->conector->insert($sql))
    	return array('ok' => 'Datos actualizados correctamente');
    else
    	return array('error' => "-Error al actualizar los datos");
}
}

?>
