<?php
require_once("../../conf/Configuracion.php");
require_once("../../jumichica/ConectorMSSQL.php");
require_once("../../mods/mod_usuarios/Usuario.php");
/**
 *
 */
class Plan_Cuentas {
	private $moduser;
	private $conector;
    function __construct() {
    	$this->moduser=new Usuario();
        $this->conector = new ConectorMSSQL(Variables::$HOST_BD, Variables::$NOMBRE_BD, Variables::$USUARIO_BD, Variables::$CLAVE_BD);
    }
    public function consultar($api, $usuario, $clave, $EmpresaId, $EmpresaNit,$rest=FALSE){
    	if($this->moduser->validarUser($usuario, $clave, $EmpresaId, $EmpresaNit)){
        	$sql="SELECT EmpresaId, PlaCueCod, PlaCueNom, PlaCueIndRecMov, PlaCueIndEst
        	FROM PLANCUENTA";
        	$datos=$this->conector->select($sql);
         	$datos=$this->conector->transformar($datos);
         	 if($rest==TRUE){
                 $datos=$this->conector->transformarRest($datos);
                 echo $datos;
            }else{
                $datos=$this->conector->transformar($datos);
                return new soapval('return', 'xsd:string', $datos);
            }
		}else{
             if($rest==TRUE){
                echo json_encode(array("error"=>"ERROR: ","respuestas"=>"Invalido algunos de estos datos: usuario, clave, empresaid o empresanit"));
            }else{
                return new soapval('return', 'xsd:string', 'Invalido algunos de estos datos: usuario, clave, empresaid o empresanit');
            }
    	}
    }
}
?>
