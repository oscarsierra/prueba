<?php
require_once("../../conf/Configuracion.php");
require_once("../../jumichica/ConectorMSSQL.php");
require_once("../../mods/mod_usuarios/Usuario.php");

/**
 * 
 */
class Tipo_Comprobante {
	private $moduser;
	private $conector;
    function __construct() {
    	$this->moduser=new Usuario();
        $this->conector = new ConectorMSSQL(Variables::$HOST_BD, Variables::$NOMBRE_BD, Variables::$USUARIO_BD, Variables::$CLAVE_BD);
    }
    
    public function crear($api, $usuario, $clave, $TipComId, $EmpresaId, $EmpresaNit, $TipComInAct,$rest=FALSE){
    	if($this->moduser->validarUser($usuario, $clave, $EmpresaId, $EmpresaNit)){
        	$sql = "INSERT INTO TIPOCOMPROBANTE (TipComId, EmpresaId, EmpresaNit, TipComInAct) 
        	VALUES ('$TipComId', '$EmpresaId', '$EmpresaNit', '$TipComInAct')";
        	$re=$this->conector->insert($sql);
            if($rest==TRUE){
                 echo $re;
            }else{
                return new soapval('return', 'xsd:string', $re);
            }
        }else{
             if($rest==TRUE){
                echo json_encode(array("error"=>"ERROR: ","respuestas"=>"Invalido algunos de estos datos: usuario, clave, empresaid o empresanit"));
             }else{
                 return new soapval('return', 'xsd:string', 'Invalido algunos de estos datos: usuario, clave, empresaid o empresanit');
             }    
    	}
    }
    
    public function modificar($api, $usuario, $clave, $TipComId, $EmpresaId, $EmpresaNit, $TipComInAct,$rest=FALSE){
         if($this->moduser->validarUser($usuario, $clave, $EmpresaId, $EmpresaNit)){	
        	$sql = "UPDATE TIPOCOMPROBANTE SET TipComId='$TipComId', EmpresaId='$EmpresaId', 
        	TipComInAct='$TipComInAct' WHERE TipComId='$TipComId', EmpresaId='$EmpresaId'";
        	$re=$this->conector->insert($sql);
            if($rest==TRUE){
                 echo $re;
            }else{
                return new soapval('return', 'xsd:string', $re);
            }
		}else{
             if($rest==TRUE){
                echo json_encode(array("error"=>"ERROR: ","respuestas"=>"Invalido algunos de estos datos: usuario, clave, empresaid o empresanit"));
             }else{
                 return new soapval('return', 'xsd:string', 'Invalido algunos de estos datos: usuario, clave, empresaid o empresanit');
             }  
    	}
    }
    
    public function consultar($api, $usuario, $clave, $TipComId, $EmpresaId, $EmpresaNit,$rest=FALSE){
       if($this->moduser->validarUser($usuario, $clave, $EmpresaId, $EmpresaNit)){	 	
        	$sql = "SELECT TipComId, EmpresaId, TipComNom, TipComNum, TipComNumResDian,
        	TipComNumFinApr, TipComNumIniApr, TipComFecApr, TipComPref, TipComInAct, ForCod, 
        	TipComIndGenCom, ForCodDoc, TipComAprDian, ModuloId, TipComGen, TipComIndVeh, 
        	TipComIndVen, TipComCodigoSucursal FROM TIPOCOMPROBANTE  
        	WHERE  EmpresaId='$EmpresaId'";
        	$datos=$this->conector->select($sql);
            if($rest==TRUE){
                 $datos=$this->conector->transformarRest($datos);
                 echo $datos;
            }else{
                $datos=$this->conector->transformar($datos);
                return new soapval('return', 'xsd:string', $datos);
            }
	   }else{
            if($rest==TRUE){
                echo json_encode(array("error"=>"ERROR: ","respuestas"=>"Invalido algunos de estos datos: usuario, clave, empresaid o empresanit"));
            }else{
                return new soapval('return', 'xsd:string', 'Invalido algunos de estos datos: usuario, clave, empresaid o empresanit');
            }
       }	 
    }
    
}


?>