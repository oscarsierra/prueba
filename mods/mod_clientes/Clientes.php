<?php
require_once("../../conf/Configuracion.php");
require_once("../../jumichica/ConectorMSSQL.php");
require_once("../../mods/mod_usuarios/Usuario.php");

/**
 *
 */
class Clientes{
	private $moduser;
	private $conector;
    function __construct() {
    	$this->moduser=new Usuario();
      $this->conector = new ConectorMSSQL(Variables::$HOST_BD, Variables::$NOMBRE_BD, Variables::$USUARIO_BD, Variables::$CLAVE_BD);
    }
public function get_clientes($empresaid){
		$sql = "SELECT
		EmpresaId,
		DirNitId,
		DirNitDigVer,
		TipDocCod,
		DirNitRazSoc,
		DirNitNom1,
		DirNitNom2,
		DirNitApe1,
		DirNitApe2,
		DirNitCon,
		DirNitDir,
		DirNitTel,
		DirNitFax,
		DirNitEmail,
		DepCod,
		MunCod,
		BarCod,
		DirNitEst,
		DirNitReg,
		DirNitPro,
		DirNitAut,
		DirNitGra,
		DirNitRetIca,
		DirNitPorRetIca,
		DirNitCli,
		DirNitCliTip,
		DirNitCliCup,
		DirNitEmp,
		DirNitSoc,
		DirPreCom,
		DirNumCom,
		ZonCod,
		DirZonaEspecial,
		DirNitSobregiro,
		DirNitListaPrecios,
		DirNitVendedor,
		DirNitForPag,
		DirNitDirEntrega,
		DirNaturaleza,
		DirActEconomica,
		DirCodPais,
		DirDeclarante
		 FROM DIRECTORIONITS WHERE EmpresaId='$empresaid'";
		$datos=$this->conector->select($sql);
		$arr=array();
		for ($i=0; $i <count($datos) ; $i++) {
			$arr[]=array(
			'EmpresaId'=>utf8_encode(trim($datos[$i][0])),
			'DirNitId'=>utf8_encode(trim($datos[$i][1])),
			'DirNitDigVer'=>utf8_encode(trim($datos[$i][2])),
			'TipDocCod'=>utf8_encode(trim($datos[$i][3])),
			'DirNitRazSoc'=>utf8_encode(trim($datos[$i][4])),
			'DirNitNom1'=>utf8_encode(trim($datos[$i][5])),
			'DirNitNom2'=>utf8_encode(trim($datos[$i][6])),
			'DirNitApe1'=>utf8_encode(trim($datos[$i][7])),
			'DirNitApe2'=>utf8_encode(trim($datos[$i][8])),
			'DirNitCon'=>utf8_encode(trim($datos[$i][9])),
			'DirNitDir'=>utf8_encode(trim($datos[$i][10])),
			'DirNitTel'=>utf8_encode(trim($datos[$i][11])),
			'DirNitFax'=>utf8_encode(trim($datos[$i][12])),
			'DirNitEmail'=>utf8_encode(trim($datos[$i][13])),
			'DepCod'=>trim($datos[$i][14]),
			'MunCod'=>trim($datos[$i][15]),
			'BarCod'=>trim($datos[$i][16]),
			'DirNitEst'=>trim($datos[$i][17]),
			'DirNitReg'=>trim($datos[$i][18]),
			'DirNitPro'=>trim($datos[$i][19]),
			'DirNitAut'=>trim($datos[$i][20]),
			'DirNitGra'=>trim($datos[$i][21]),
			'DirNitRetIca'=>trim($datos[$i][22]),
			'DirNitPorRetIca'=>trim($datos[$i][23]),
			'DirNitCli'=>utf8_encode(trim($datos[$i][24])),
			'DirNitCliTip'=>utf8_encode(trim($datos[$i][25])),
			'DirNitCliCup'=>utf8_encode(trim($datos[$i][26])),
			'DirNitEmp'=>utf8_encode(trim($datos[$i][27])),
			'DirNitSoc'=>utf8_encode(trim($datos[$i][28])),
			'DirPreCom'=>utf8_encode(trim($datos[$i][29])),
			'DirNumCom'=>trim($datos[$i][30]),
			'ZonCod'=>trim($datos[$i][31]),
			'DirZonaEspecial'=>trim($datos[$i][32]),
			'DirNitSobregiro'=>trim($datos[$i][33]),
			'DirNitListaPrecios'=>trim($datos[$i][34]),
			'DirNitVendedor'=>trim($datos[$i][35]),
			'DirNitForPag'=>trim($datos[$i][36]),
			'DirNitDirEntrega'=>trim($datos[$i][37]),
			'DirNaturaleza'=>trim($datos[$i][38]),
			'DirActEconomica'=>trim($datos[$i][39]),
			'DirCodPais'=>trim($datos[$i][40]),
			'DirDeclarante'=>trim($datos[$i][41])
		);
		}
  return $arr;
	//	return $arr;
  }
public function set_clientes($EmpresaIdN,$DirNitIdN,$DirNitDigVer,$TipDocCod,$DirNitRazSoc,$DirNitNom1,$DirNitNom2,$DirNitApe1,$DirNitApe2,$DirNitCon,$DirNitDir,$DirNitTel,$DirNitFax,$DirNitEmail,
	$DepCod,$MunCod,$BarCod,$DirNitEst,$DirNitReg,$DirNitPro,$DirNitAut,$DirNitGra,$DirNitRetIca,$DirNitPorRetIca,$DirNitCli,$DirNitCliTip,$DirNitCliCup,$DirNitEmp,$DirNitSoc,$DirPreCom,$DirNumCom,$ZonCod,
	$DirZonaEspecial,$DirNitSobregiro,$DirNitListaPrecios,$DirNitVendedor,$DirNitForPag,$DirNitDirEntrega,$DirNaturaleza,$DirActEconomica,$DirCodPais,$DirDeclarante){ //DirNitId
		$sql = "INSERT INTO DIRECTORIONITS (EmpresaId,DirNitId,DirNitDigVer,TipDocCod,DirNitRazSoc,DirNitNom1,DirNitNom2,DirNitApe1,DirNitApe2,DirNitCon,DirNitDir,DirNitTel,DirNitFax,DirNitEmail,
			DepCod,MunCod,BarCod,DirNitEst,DirNitReg,DirNitPro,DirNitAut,DirNitGra,DirNitRetIca,DirNitPorRetIca,DirNitCli,DirNitCliTip,DirNitCliCup,DirNitEmp,DirNitSoc,DirPreCom,DirNumCom,ZonCod,
			DirZonaEspecial,DirNitSobregiro,DirNitListaPrecios,DirNitVendedor,DirNitForPag,DirNitDirEntrega,DirNaturaleza,DirActEconomica,DirCodPais,DirDeclarante)
    VALUES ('$EmpresaIdN','$DirNitIdN','$DirNitDigVer','$TipDocCod','$DirNitRazSoc','$DirNitNom1','$DirNitNom2','$DirNitApe1','$DirNitApe2','$DirNitCon','$DirNitDir','$DirNitTel','$DirNitFax','$DirNitEmail',
			'$DepCod','$MunCod','$BarCod','$DirNitEst','$DirNitReg','$DirNitPro','$DirNitAut','$DirNitGra','$DirNitRetIca','$DirNitPorRetIca','$DirNitCli','$DirNitCliTip','$DirNitCliCup','$DirNitEmp','$DirNitSoc','$DirPreCom','$DirNumCom','$ZonCod',
			'$DirZonaEspecial','$DirNitSobregiro','$DirNitListaPrecios','$DirNitVendedor','$DirNitForPag','$DirNitDirEntrega','$DirNaturaleza','$DirActEconomica','$DirCodPais','$DirDeclarante')";

		if($this->conector->insert($sql))
    	return array('ok' => "Datos guardados correctamente" );
    else
      return array('error' => "Error al guardar los datos");

  }
	public function update_clientes($EmpresaIdN,$DirNitIdN,$DirNitDigVer,$TipDocCod,$DirNitRazSoc,$DirNitNom1,$DirNitNom2,$DirNitApe1,$DirNitApe2,$DirNitCon,$DirNitDir,$DirNitTel,$DirNitFax,$DirNitEmail,
			$DepCod,$MunCod,$BarCod,$DirNitEst,$DirNitReg,$DirNitPro,$DirNitAut,$DirNitGra,$DirNitRetIca,$DirNitPorRetIca,$DirNitCli,$DirNitCliTip,$DirNitCliCup,$DirNitEmp,$DirNitSoc,$DirPreCom,$DirNumCom,$ZonCod,
			$DirZonaEspecial,$DirNitSobregiro,$DirNitListaPrecios,$DirNitVendedor,$DirNitForPag,$DirNitDirEntrega,$DirNaturaleza,$DirActEconomica,$DirCodPais,$DirDeclarante){ //DirNitId
			$sql = "UPDATE  DIRECTORIONITS SET  DirNitDigVer='$DirNitDigVer',TipDocCod='$TipDocCod',DirNitRazSoc='$DirNitRazSoc',
			DirNitNom1='$DirNitNom1',DirNitNom2='$DirNitNom2',DirNitApe1='$DirNitApe1',DirNitApe2='$DirNitApe2',DirNitCon='$DirNitCon',DirNitDir='$DirNitDir',
			DirNitTel='$DirNitTel',DirNitFax='$DirNitFax',DirNitEmail='$DirNitEmail',DepCod='$DepCod',MunCod='$MunCod',BarCod='$BarCod',
			DirNitEst='$DirNitEst',DirNitReg='$DirNitReg',DirNitPro='$DirNitPro',DirNitAut='$DirNitAut',DirNitGra='$DirNitGra',
			DirNitRetIca='$DirNitRetIca',DirNitPorRetIca='$DirNitPorRetIca',DirNitCli='$DirNitCli',DirNitCliTip='$DirNitCliTip',
			DirNitCliCup='$DirNitCliCup',DirNitEmp='$DirNitEmp',DirNitSoc='$DirNitSoc',DirPreCom='$DirPreCom',DirNumCom='$DirNumCom',ZonCod='$ZonCod',
			DirZonaEspecial='$DirZonaEspecial',DirNitSobregiro='$DirNitSobregiro',DirNitListaPrecios='$DirNitListaPrecios',DirNitVendedor='$DirNitVendedor',
			DirNitForPag='$DirNitForPag',DirNitDirEntrega='$DirNitDirEntrega',DirNaturaleza='$DirNaturaleza',DirActEconomica='$DirActEconomica',
			DirCodPais='$DirCodPais',DirDeclarante='$DirDeclarante' WHERE DirNitId='$DirNitIdN' AND EmpresaId='$EmpresaIdN'";
      // echo "string".$sql;
			if($this->conector->insert($sql))
	   		return array('ok' => "Datos actualizados correctamente");
	    else
	      return array('error' => "Error al actualizar los datos");
	  }
}

?>
