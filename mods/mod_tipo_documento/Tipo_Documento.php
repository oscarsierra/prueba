<?php
require_once("../../conf/Configuracion.php");
require_once("../../jumichica/ConectorMSSQL.php");
require_once("../../mods/mod_usuarios/Usuario.php");
/**
 * 
 */
class Tipo_Documento {
	private $moduser;
	private $conector;
    function __construct() {
    	$this->moduser=new Usuario();
        $this->conector = new ConectorMSSQL(Variables::$HOST_BD, Variables::$NOMBRE_BD, Variables::$USUARIO_BD, Variables::$CLAVE_BD);
    }
    public function crear($api, $usuario, $clave, $TipDocCod,$TipDocNom,$rest=FALSE){
    	if($this->moduser->validarUserOnly($usuario, $clave)){
        	$sql="INSERT INTO TIPODOCUMENTO (TipDocCod,TipDocNom) VALUES('$TipDocCod','$TipDocNom')";
        	$re=$this->conector->insert($sql);
        	if($rest==TRUE){
                 echo $re;
            }else{
                return new soapval('return', 'xsd:string', $re);
            }
		}else{
    		if($rest==TRUE){
                echo json_encode(array("error"=>"ERROR: ","respuestas"=>'Invalido algunos de estos datos: usuario o clave'));
            }else{
                 return new soapval('return', 'xsd:string', 'Invalido algunos de estos datos: usuario o clave');
            }
    	}	
		
    }
    public function consultar($api, $usuario, $clave, $TipDocCod,$rest=FALSE){
    	if($this->moduser->validarUserOnly($usuario, $clave)){
        	$sql="SELECT TipDocCod,TipDocNom FROM TIPODOCUMENTO WHERE TipDocCod = '$TipDocCod'";
       		$datos=$this->conector->select($sql);
         	 if($rest==TRUE){
                 $datos=$this->conector->transformarRest($datos);
                 echo $datos;
            }else{
                $datos=$this->conector->transformar($datos);
                return new soapval('return', 'xsd:string', $datos);
            }
		}else{
    		 if($rest==TRUE){
                echo json_encode(array("error"=>"ERROR: ","respuestas"=>'Invalido algunos de estos datos: usuario o clave'));
            }else{
                 return new soapval('return', 'xsd:string', 'Invalido algunos de estos datos: usuario o clave');
            }
    	}	
    }
    
}

?>