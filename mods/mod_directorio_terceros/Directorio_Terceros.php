<?php
require_once("../../conf/Configuracion.php");
require_once("../../jumichica/ConectorMSSQL.php");
require_once("../../mods/mod_usuarios/Usuario.php");
/**
 * 
 */
class Directorio_Terceros {
	
	private $moduser;
	private $conector;
    function __construct() {
    	$this->moduser=new Usuario();
        $this->conector = new ConectorMSSQL(Variables::$HOST_BD, Variables::$NOMBRE_BD, Variables::$USUARIO_BD, Variables::$CLAVE_BD);
    }
    
    public function crear($api , $usuario , $clave , 
         $EmpresaId , $EmpresaNit , $DirNitId , $DirNitDigVer , $TipDocCod 
         , $DirNitRazSoc , $DirNitNom1 , $DirNitNom2 , $DirNitApe1 , $DirNitApe2 
         , $DirNitCon , $DirNitDir , $DirNitTel , $DirNitEmail 
         , $DepCod , $MunCod , $DirNitEst , $DirNitReg , $DirNitpro 
         , $DirNitAut , $DirNitGra , $DirNitCli , $DirNitEmp , $DirNitSoc 
         , $DirNaturalezaJuridico , $DirActEconomica , $DirCodPais , $DirDeclarante,$rest=FALSE){
        if($this->moduser->validarUser($usuario, $clave, $EmpresaId, $EmpresaNit)){     
        	$sql = "INSERT INTO DIRECTORIONITS (EmpresaId , DirNitId , DirNitDigVer , TipDocCod 
         	, DirNitRazSoc , DirNitNom1 , DirNitNom2 , DirNitApe1 , DirNitApe2 
         	, DirNitCon , DirNitDir , DirNitTel , DirNitEmail 
         	, DepCod , MunCod , DirNitEst , DirNitReg , DirNitpro 
         	, DirNitAut , DirNitGra , DirNitCli , DirNitEmp , DirNitSoc 
         	, DirNaturaleza , DirActEconomica , DirCodPais , DirDeclarante, BarCod, DirNitRetIca, DirNitPorRetIca,
         	DirNitCliTip, DirNitCliCup, DirPreCom, DirNumCom, ZonCod, DirZonaEspecial, DirNitSobregiro, DirNitListaPrecios) 
         	VALUES('$EmpresaId' ,  '$DirNitId' , '$DirNitDigVer' , '$TipDocCod' 
         	, '$DirNitRazSoc' , '$DirNitNom1' , '$DirNitNom2' , '$DirNitApe1' , '$DirNitApe2' 
         	, '$DirNitCon' , '$DirNitDir' , '$DirNitTel' , '$DirNitEmail' 
        	 , '$DepCod' , '$MunCod' , '$DirNitEst' , '$DirNitReg' , '$DirNitpro' 
         	, '$DirNitAut' , '$DirNitGra' , '$DirNitCli' , '$DirNitEmp' , '$DirNitSoc' 
         	, '$DirNaturalezaJuridico' , '$DirActEconomica' , '$DirCodPais' , '$DirDeclarante',
        	 '00001', '0', '0', 'C', '0', null, '0', '00', '0', '0', null)"; 
         
         	$dat=$this->conector->insert($sql);
            if($rest==TRUE){
                 $dat=$this->conector->transformarRest($dat);
                 echo $dat;
            }else{
                $dat=$this->conector->transformar($dat);
                return new soapval('return', 'xsd:string', $dat);
            }
        }else{
            if($rest==TRUE){
               echo json_encode(array("error"=>"ERROR: ","respuestas"=>"Invalido algunos de estos datos: usuario, clave, empresaid o empresanit"));
            }else{
                return new soapval('return', 'xsd:string', 'Invalido algunos de estos datos: usuario, clave, empresaid o empresanit');
            }
        }
    }
    
    public function modificar($api , $usuario , $clave , 
         $EmpresaId , $EmpresaNit , $DirNitId , $DirNitDigVer , $TipDocCod 
         , $DirNitRazSoc , $DirNitNom1 , $DirNitNom2 , $DirNitApe1 , $DirNitApe2 
         , $DirNitCon , $DirNitDir , $DirNitTel , $DirNitEmail 
         , $DepCod , $MunCod , $DirNitEst , $DirNitReg , $DirNitpro 
         , $DirNitAut , $DirNitGra , $DirNitCli , $DirNitEmp , $DirNitSoc 
         , $DirNaturalezaJuridico , $DirActEconomica , $DirCodPais , $DirDeclarante,$rest=FALSE){
           if($this->moduser->validarUser($usuario, $clave, $EmpresaId, $EmpresaNit)){  
       	 	 $sql = "UPDATE DIRECTORIONITS SET  
       		 , DirNitDigVer='$DirNitDigVer' , TipDocCod='$TipDocCod' 
         	 , DirNitRazSoc='$DirNitRazSoc' , DirNitNom1='$DirNitNom1' , DirNitNom2='$DirNitNom2' , 
        	 DirNitApe1='$DirNitApe1' , DirNitApe2='$DirNitApe2' 
         	 , DirNitCon='$DirNitCon' , DirNitDir='$DirNitDir' , DirNitTel='$DirNitTel' , DirNitEmail= '$DirNitEmail'
         	 , DepCod='$DepCod' , MunCod='$MunCod' , DirNitEst='$DirNitEst' , DirNitReg='$DirNitReg' , DirNitpro='$DirNitpro' 
         	 , DirNitAut='$DirNitAut' , DirNitGra='$DirNitGra' , DirNitCli='$DirNitCli' , DirNitEmp='$DirNitEmp' , DirNitSoc='$DirNitSoc' 
         	 , DirNaturaleza='$DirNaturalezaJuridico' , DirActEconomica='$DirActEconomica' , DirCodPais='$DirCodPais' 
         	 , DirDeclarante='$DirDeclarante' WHERE EmpresaId='$EmpresaId' AND DirNitId='$DirNitId'";
         
         	 $dat=$this->conector->insert($sql);
            if($rest==TRUE){
                 $dat=$this->conector->transformarRest($dat);
                 echo $dat;
            }else{
                $dat=$this->conector->transformar($dat);
                return new soapval('return', 'xsd:string', $dat);
            }
        }else{
            if($rest==TRUE){
               echo json_encode(array("error"=>"ERROR: ","respuestas"=>"Invalido algunos de estos datos: usuario, clave, empresaid o empresanit"));
            }else{
                return new soapval('return', 'xsd:string', 'Invalido algunos de estos datos: usuario, clave, empresaid o empresanit');
            }
        }
    }
    
    public function consultar($api , $usuario , $clave , $EmpresaId , $EmpresaNit , $DirNitId ,$rest=FALSE){
       if($this->moduser->validarUser($usuario, $clave, $EmpresaId, $EmpresaNit)){      
        	$sql = "SELECT EmpresaId ,  DirNitId , DirNitDigVer , TipDocCod 
        	 , DirNitRazSoc , DirNitNom1 , DirNitNom2 , DirNitApe1 , DirNitApe2 
         	, DirNitCon , DirNitDir , DirNitTel , DirNitEmail 
        	 , DepCod , MunCod , DirNitEst , DirNitReg , DirNitpro 
        	 , DirNitAut , DirNitGra , DirNitCli , DirNitEmp , DirNitSoc 
        	 , DirNaturaleza , DirActEconomica , DirCodPais , DirDeclarante, BarCod, DirNitRetIca, DirNitPorRetIca,
        	 DirNitCliTip, DirNitCliCup, DirPreCom, DirNumCom, ZonCod, DirZonaEspecial, DirNitSobregiro, DirNitListaPrecios 
       		 FROM DIRECTORIONITS WHERE EmpresaId='$EmpresaId' "; 
         
        	 $datos=$this->conector->select($sql);
                if($rest==TRUE){
                     $datos=$this->conector->transformarRest($datos);
                     echo $datos;
                }else{
                    $datos=$this->conector->transformar($datos);
                    return new soapval('return', 'xsd:string', $datos);
                }
         }else{
            if($rest==TRUE){
               echo json_encode(array("error"=>"ERROR: ","respuestas"=>"Invalido algunos de estos datos: usuario, clave, empresaid o empresanit"));
            }else{
                return new soapval('return', 'xsd:string', 'Invalido algunos de estos datos: usuario, clave, empresaid o empresanit');
            }
        }
    }
    
}


?>