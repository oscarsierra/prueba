<?php
require_once("../../conf/Configuracion.php");
require_once("../../jumichica/ConectorMSSQL.php");
require_once("../../mods/mod_usuarios/Usuario.php");

/**
 *
 */
class Dxn{
	private $moduser;
	private $conector;
    function __construct() {
    	$this->moduser=new Usuario();
			$this->conector2= new ConectorMSSQL(Variables::$HOST_BD, Variables::$NOMBRE_BD2, Variables::$USUARIO_BD2, Variables::$CLAVE_BD2);
     }
public function get_dxn($empresaid){

	$sql = "SELECT     e.EmpleNumDoc, nc.NomConcepVlr, nc.PeriodoId, CONVERT(char(10), pl.PeriodoHasta, 126) AS Expr1
			FROM         NominaConceptos AS nc INNER JOIN
                      Contratos AS c ON nc.NominaContID = c.ContID AND nc.EmpIde = c.EmpIde INNER JOIN
                      Empleados AS e ON c.ContEmpleID = e.EmpleID AND nc.EmpIde = e.EmpIde INNER JOIN
                      PeriodosLiq AS pl ON nc.PeriodoId = pl.PeriodoId AND nc.TNominaID = pl.TNominaID AND pl.EmpIde = nc.EmpIde
			WHERE     (nc.NomConcepID = 99) AND (nc.EmpIde = '$empresaid') AND (c.ContEstado = 'A')";

		$datos=$this->conector2->select($sql);
		$arr=array();
		for ($i=0; $i <count($datos) ; $i++) {
				$arr[]=array(
					'EmpleNumDoc' => trim(utf8_encode($datos[$i][0])),
					'NomValor'=>utf8_encode($datos[$i][1]),
					'NomPeriodo'=>$datos[$i][2],
					'NomVigencia'=>$datos[$i][3]
				);
		}
		return $arr;
  }
	public function get_dxn_fecha($empresaid,$year,$month){
		if(strlen($month)==1)
		 $month="0".$month;
		
		$sql = "SELECT     e.EmpleNumDoc, nc.NomConcepVlr, nc.PeriodoId, CONVERT(char(10), pl.PeriodoHasta, 126) AS Expr1
		FROM         NominaConceptos AS nc INNER JOIN
				Contratos AS c ON nc.NominaContID = c.ContID AND nc.EmpIde = c.EmpIde INNER JOIN
				Empleados AS e ON c.ContEmpleID = e.EmpleID AND nc.EmpIde = e.EmpIde INNER JOIN
				PeriodosLiq AS pl ON nc.PeriodoId = pl.PeriodoId AND nc.TNominaID = pl.TNominaID AND pl.EmpIde = nc.EmpIde
		WHERE     (nc.NomConcepID = 99) AND (nc.EmpIde = '$empresaid') AND (c.ContEstado = 'A')  AND (nc.PeriodoId LIKE '".$year.$month."%')";


			$datos=$this->conector2->select($sql);
			$arr=array();
			for ($i=0; $i <count($datos) ; $i++) {
					$arr[]=array(
						'EmpleNumDoc' => trim(utf8_encode($datos[$i][0])),
						'NomValor'=>utf8_encode($datos[$i][1]),
						'NomPeriodo'=>$datos[$i][2],
						'NomVigencia'=>$datos[$i][3]
					);
			}
			return $arr;
	  }
}

?>
