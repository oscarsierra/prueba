<?php
require_once("../../conf/Configuracion.php");
require_once("../../jumichica/ConectorMSSQL.php");
require_once("../../mods/mod_usuarios/Usuario.php");

/**
 *
 */
class Contabilizacion {
	private $moduser;
	private $conector;
	private $conector2;
    function __construct() {
    	$this->moduser=new Usuario();
        $this->conector = new ConectorMSSQL(Variables::$HOST_BD, Variables::$NOMBRE_BD, Variables::$USUARIO_BD, Variables::$CLAVE_BD);
          $this->conector2 = new ConectorMSSQL(Variables::$HOST_BD2, Variables::$NOMBRE_BD2, Variables::$USUARIO_BD2, Variables::$CLAVE_BD2);
    }
    public function get_contabilizacion($empresaid,$ComNun){
				$sql = "SELECT  c.COMNUM,
				CASE WHEN cd.CENCOSID ='000' OR cd.CENCOSID =''  THEN '11' ELSE cd.CENCOSID END AS Sucursal,
				c.TIPCOMID, CONVERT(char(10), c.COMFEC, 126),c.COMCON,cd.PLACUECOD,cd.COMTIPMOV,cd.COMCONDET,cd.COMVAL,cd.COMNITDET,
				'',cd.COMDOCSOP,cd.COMVALRET,'',
				CASE WHEN cd.CENCOSID ='000' OR cd.CENCOSID =''  THEN '11' ELSE cd.CENCOSID END AS SucursalAUX,
				c.COMANU,c.ComCambio
				FROM COMPROBANTE AS c, COMPROBANTEDETALLE AS cd
				WHERE c.COMNUM=cd.COMNUM AND c.EmpresaId=cd.EmpresaId AND c.TipComId=cd.TipComId 
				 AND c.ComNum='$ComNun' AND c.EmpresaId=$empresaid";
				$datos=$this->conector->select($sql);
				$arr=array();
				for ($i=0; $i <count($datos) ; $i++) {
					$ComCambio=$datos[$i][16];
					$COMANU=$datos[$i][15];
					if($ComCambio==1){
						if($COMANU==1){
							$tipoOpe="A";
						}else{
							$tipoOpe="M";
						}
					}else{
						$tipoOpe="C";
					}

					   $arr[]=array(
							 'NumDoc' =>  trim(utf8_encode($datos[$i][0])),
							 'Sucursal' =>  trim(utf8_encode(Variables::$SUCURSAL_CONTABILIZACION)),
							 'CodDoc' =>  trim(utf8_encode($datos[$i][2])),
							 'Fecha' =>  trim(utf8_encode($datos[$i][3])),
							 'DetalleGen' =>trim(utf8_encode($datos[$i][4])),
							 'Cuenta' =>  trim($datos[$i][5]),
							 'Naturaleza' =>  trim(utf8_encode($datos[$i][6])),
							 'DetallePop' =>  trim(utf8_encode($datos[$i][7])),
							 'ValorTran' =>  trim($datos[$i][8]),
							 'Nit' =>  trim($datos[$i][9]),
							 'CentroCosto' =>  ($datos[$i][10]),
							 'Cheque' =>  trim($datos[$i][11]),
							 'Base' =>  trim($datos[$i][12]),
							 'CodActivoFijo' =>  (utf8_encode($datos[$i][13])),
							 'SucursalAux' =>  trim(utf8_encode(Variables::$SUCURSAL_CONTABILIZACION)),
							 'TipoOpe' =>  $tipoOpe
						 );
				}
				return $arr;
  }


	public function get_contabilizacion_fecha($empresaid,$ano,$mes){
		  if(strlen($mes)==1)
				$mes="0".$mes;
		  $fecha=$ano."-".$mes;
			$sql = "SELECT  c.COMNUM,
			 CASE WHEN cd.CENCOSID ='000' OR cd.CENCOSID =''  THEN '11' ELSE cd.CENCOSID END AS Sucursal,
			 c.TIPCOMID, CONVERT(char(10), c.COMFEC, 126),c.COMCON,cd.PLACUECOD,cd.COMTIPMOV,cd.COMCONDET,
			 cd.COMVAL,cd.COMNITDET,'',cd.COMDOCSOP,cd.COMVALRET,'',
			 CASE WHEN cd.CENCOSID ='000' OR cd.CENCOSID =''  THEN '11' ELSE cd.CENCOSID END AS SucursalAUX
			 ,c.COMANU,c.ComCambio
			FROM COMPROBANTE AS c, COMPROBANTEDETALLE AS cd
			WHERE c.COMNUM=cd.COMNUM AND c.EmpresaId=cd.EmpresaId AND c.TipComId=cd.TipComId  AND MONTH(c.COMFEC)='$mes' AND  YEAR(c.COMFEC)='$ano' AND c.EmpresaId=$empresaid";
			$datos=$this->conector->select($sql);
			$arr=array();
			for ($i=0; $i <count($datos) ; $i++) {
				$ComCambio=$datos[$i][16];
				$COMANU=$datos[$i][15];
				if($ComCambio==1){
					if($COMANU==1){
						$tipoOpe="A";
					}else{
						$tipoOpe="M";
					}
				}else{
					$tipoOpe="C";
				}

				$arr[]=array(
					'NumDoc' =>  trim(utf8_encode($datos[$i][0])),
					'Sucursal' =>  trim(utf8_encode(Variables::$SUCURSAL_CONTABILIZACION)),
					'CodDoc' =>  trim(utf8_encode($datos[$i][2])),
					'Fecha' =>  trim(utf8_encode($datos[$i][3])),
					'DetalleGen' =>trim(utf8_encode($datos[$i][4])),
					'Cuenta' =>  trim($datos[$i][5]),
					'Naturaleza' =>  trim(utf8_encode($datos[$i][6])),
					'DetallePop' =>  trim(utf8_encode($datos[$i][7])),
					'ValorTran' =>  trim($datos[$i][8]),
					'Nit' =>  trim($datos[$i][9]),
					'CentroCosto' =>  ($datos[$i][10]),
					'Cheque' =>  trim($datos[$i][11]),
					'Base' =>  trim($datos[$i][12]),
					'CodActivoFijo' =>  (utf8_encode($datos[$i][13])),
					'SucursalAux' =>  trim(utf8_encode(Variables::$SUCURSAL_CONTABILIZACION)),
					'TipoOpe' =>  $tipoOpe
				);
			}
			return $arr;
	}


	public function PeriodoLiqCob($id_empresa){
	    $sql="select substring(periodoid,0,5)+'/'+substring(periodoid,5,2) as PerLiq,
				substring(periodoid,6,1) as PerCorte ,periodoid
				from periodosliq where periodocambio is null 
				AND empide=".$id_empresa;
		
		$datos=$this->conector2->select($sql);
		return $datos;
	}

	public function PeriodoLiqNot($id_empresa){
		$sql="	select periodoId,TNominaId
		from periodosliq where periodocam is null 
		AND empide=".$id_empresa;
		$datos=$this->conector2->select($sql);
		return $datos;
	}

	public function CobroDxN($perLiq,$perCorte){
			$jsonData = array(
				'perCorte'=>"2",
				'perLiq'=>$perLiq
			);
		    $jsonDataEncoded=json_encode($jsonData);
		    $ch = curl_init();
		    $header = array();
		    $header[] = 'Content-length: '.strlen($jsonDataEncoded);
		    $header[] = 'Content-Type: application/json';
		    $header[] = 'Authorization: '.Variables::$AUTHORIZATIONXEO;
		    curl_setopt( $ch, CURLOPT_HTTPHEADER, $header );
		    curl_setopt( $ch, CURLOPT_URL, Variables::$URLXEO."CobroDxN" );
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
		    curl_setopt( $ch, CURLOPT_POSTFIELDS,  $jsonDataEncoded);
		    curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
		    $server_output = curl_exec ( $ch );
		    curl_close ( $ch );
		    return $server_output; 
	}

	public function migrarDxN($data,$id_empresa){

		$sql=  "SELECT TOP 1 CONTID
				FROM empleados e, contratos c
				WHERE e.empleid=c.contempleid
				AND emplenumdoc=".$data->docIde."
				AND c.empide=".$id_empresa."
				AND contestado='A'
				ORDER BY CONTSALARIO DESC";
		$datos=$this->conector2->select($sql);
		
		$sql2= "select max(ContConcepID) ContConcepID
				from ContratosConceptos
				where contid=".$datos[0][0];
		$datos2=$this->conector2->select($sql2);
		$conceptid=$datos2[0][0]+1;

		if(!empty($datos)){
			$insertMet="insert into ContratosConceptos
					    (EmpIde,ContID,ContConcepID,ContConceptoID,ContConcepValor,ContConcepPorc,
					    ContConcepFchCrea,ContConcepUsuCod,ContConcepVigen,ContConcepDet,
					    ContTerceroID,ContConcepAB,ContConcepNC)
					    values (".$id_empresa." , ".$datos[0][0].",'".$conceptid."','99',".
					    $data->valCobrar.",0,SYSDATETIME() ,'13',NULL,'".$data->desDed."',NULL,NULL,1)";
		
		  $this->conector2->insert($insertMet);
			return true;
		}else{
			return false;
		}
	}

	public function actualizarPeriodoCambio($id_empresa,$periodoid){
		$updateMet="UPDATE PERIODOSLIQ set PERIODOCAMBIO=0 
					where empide=".$id_empresa." and periodoid=".$periodoid;
		if($this->conector2->update($updateMet)){
			return true;
		}else{
			return false;
		}
	}

	public function RegistrarPagosDxN($jsonDataEncoded){
	    $ch = curl_init();
	    $header = array();
	    $header[] = 'Content-length: '.strlen($jsonDataEncoded);
	    $header[] = 'Content-Type: application/json';
	   	$header[] = 'Authorization: '.Variables::$AUTHORIZATIONXEO;
	    curl_setopt( $ch, CURLOPT_HTTPHEADER, $header );
	    curl_setopt( $ch, CURLOPT_URL, Variables::$URLXEO."RegistrarPagosDxN" );
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
	    curl_setopt( $ch, CURLOPT_POSTFIELDS,  $jsonDataEncoded);
	    curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
	    $server_output = curl_exec ( $ch );
	    curl_close ( $ch );
	    return $server_output;
	}
	public function actualizarPeriodoCamb($id_empresa,$periodoid){
		$updateMet="UPDATE PERIODOSLIQ set PERIODOCAM=0 
					where empide=".$id_empresa." and periodoid=".$periodoid;
		if($this->conector2->update($updateMet)){
			return true;
		}else{
			return false;
		}
	}

	public function notificarCxN($periodoid,$nominaid,$id_empresa){
		$sql= "select e.emplenumdoc,nc.nomconcepvlr,nc.periodoid
				from nominaconceptos nc, contratos c,empleados e
				where nc.tnominaid=".$nominaid."
				and nc.empide=".$id_empresa."
				and nc.nomconcepid=99
				and nc.nominacontid=c.contid
				and e.empleid=c.contempleid
				AND nc.EmpIde=c.EmpIde
				and e.EmpIde=nc.EmpIde
				AND contestado='A'
				and nc.periodoid='".$periodoid."'";
		$datos=$this->conector2->select($sql);
		$array=array();
		if(!empty($datos)){
			for ($i=0; $i <count($datos); $i++) {
				$fechaPago=substr($datos[$i][2], 6,2);
				if($fechaPago=="00" || $fechaPago=="02"){
					$dia= str_pad(date("d",(mktime(0,0,0,substr($datos[$i][2], 4,2)+1,1,substr($datos[$i][2], 0,4))-1)), 2, "0", STR_PAD_LEFT);
					$fechaPago=substr($datos[$i][2], 0,6).$dia;
				}elseif($fechaPago=="01"){
					$fechaPago=substr($datos[$i][2], 0,6)."15";
				}
				array_push($array, array("docIde"=>trim($datos[$i][0]),"valorPago"=>$datos[$i][1],"fechaPago"=>$fechaPago));
			}
			$respuesta=array("status"=>"200","data"=>$array);
		}else{
			$respuesta=array("status"=>"Nomina no tiene empleados","data"=>$array);
		}
		return  json_encode($respuesta);
	}

	public function RegistrarContabilizacion($jsonDataEncoded){
	    $ch = curl_init();
	    $header = array();
	    $header[] = 'Content-length: '.strlen($jsonDataEncoded);
	    $header[] = 'Content-Type: application/json';
		$header[] = 'Authorization: '.Variables::$AUTHORIZATIONXEO;
	    curl_setopt( $ch, CURLOPT_HTTPHEADER, $header );
	    curl_setopt( $ch, CURLOPT_URL, Variables::$URLXEO."RegistrarContabilizacion" );
		  curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
	    curl_setopt( $ch, CURLOPT_POSTFIELDS,  $jsonDataEncoded);
	    curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
	    $server_output = curl_exec ( $ch );
	    curl_close ( $ch );
	    return $server_output;
	}
	 public function update_comprobante($idEmpresa,$comnum,$TipComId){
		 $sql="UPDATE COMPROBANTE SET ComCambio=0 WHERE EmpresaId=$idEmpresa AND ComNum='$comnum' AND TipComId='$TipComId'";
		 $this->conector->update($sql);
	 }
   public function get_contabilizacion2($idEmpresa){
		 $sql="	SELECT ComNum, EmpresaId, TipComId, ComFec, ComNit, ComCon, ComPerAgno, ComPerMes, 
		 		ComUltCons, ComAnu, ComUsuarioId, ComCambio
 				FROM COMPROBANTE WHERE (EmpresaId = $idEmpresa) AND ( ComCambio is null or ComCambio !=0 )";

		 $datos=$this->conector->select($sql);
		 $arr=array();
		 for ($i=0; $i <count($datos) ; $i++) {
			 	$arr[]=array("EmpresaId"=>$idEmpresa,"ComNum"=>$datos[$i][0],"TipComId"=>$datos[$i][2]);
		 }
		 return $arr;
	}
	
   public function migrar_contabilizacion($idEmpresa,$comnum,$TipComId){
		$sql = "SELECT  c.COMNUM, CASE WHEN cd.CENCOSID ='000' OR cd.CENCOSID =''  THEN '11' ELSE cd.CENCOSID END AS Sucursal,
		c.TIPCOMID, CONVERT(char(10), c.COMFEC, 126),c.COMCON,cd.PLACUECOD,cd.COMTIPMOV,cd.COMCONDET,
		cd.COMVAL,cd.COMNITDET,'',cd.COMDOCSOP,cd.COMVALRET,'',
		CASE WHEN cd.CENCOSID ='000' OR cd.CENCOSID =''  THEN '11' ELSE cd.CENCOSID END AS SucursalAUX,
		c.COMANU,c.ComCambio
		FROM COMPROBANTE AS c, COMPROBANTEDETALLE AS cd
		WHERE c.COMNUM=cd.COMNUM AND c.EmpresaId=cd.EmpresaId AND c.TipComId=cd.TipComId 
		AND  c.EmpresaId=$idEmpresa AND c.ComNum='$comnum' AND c.TipComId='$TipComId'";
		$datos=$this->conector->select($sql);
		$arr=array();
		for ($i=0; $i <count($datos) ; $i++) {

			$ComCambio=$datos[$i][16];
			$COMANU=$datos[$i][15];
			if($ComCambio==1){
				if($COMANU==1){
					$tipoOpe="A";
				}else{
					$tipoOpe="M";
				}
			}else{
				$tipoOpe="C";
			}

			   $arr[]=array(
					 'numDocCon' =>  trim(utf8_encode(substr($datos[$i][0], 2, 10))),
					 'codSuc' =>  trim(utf8_encode(Variables::$SUCURSAL_CONTABILIZACION)),
					 'codDocCon' =>  trim(utf8_encode($datos[$i][2])),
					 'fecha' =>  trim(utf8_encode($datos[$i][3])),
					 'detalleGen' =>trim(utf8_encode($datos[$i][4])),
					 'cuenta' =>  trim($datos[$i][5]),
					 'naturaleza' =>  trim(utf8_encode($datos[$i][6])),
					 'detallePar' =>  trim(utf8_encode(substr($datos[$i][7], 0,49))),
					 'valorTran' =>  trim($datos[$i][8]),
					 'nit' =>  trim($datos[$i][9]),
					 'centroCosto' =>  ($datos[$i][10]),
					 'cheque' =>  trim($datos[$i][11]),
					 'valorBase' =>  trim($datos[$i][12]),
					 'codActFijo' =>  (utf8_encode($datos[$i][13])),
					 'sucAux' =>  trim(utf8_encode(Variables::$SUCURSAL_CONTABILIZACION)),
					 'tipoOpe' =>  $tipoOpe
				 );
				 
		}
		return $arr;
  }

	public function RegistrarClientes($jsonDataEncoded){
	    $ch = curl_init();
	    $header = array();
	    $header[] = 'Content-length: '.strlen($jsonDataEncoded);
	    $header[] = 'Content-Type: application/json';
	   	$header[] = 'Authorization: '.Variables::$AUTHORIZATIONXEO;
	    curl_setopt( $ch, CURLOPT_HTTPHEADER, $header );
	    curl_setopt( $ch, CURLOPT_URL, Variables::$URLXEO."RegistrarClientes" );
		  curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
	    curl_setopt( $ch, CURLOPT_POSTFIELDS,  $jsonDataEncoded);
	    curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
	    $server_output = curl_exec ( $ch );
	    curl_close ( $ch );
	    return $server_output;
	}

   	public function migrar_clientes($idEmpresa){
		$sql=  "SELECT EmpresaId,DirNitId,DirNitDigVer,TipDocCod,DirNitRazSoc,DirNitNom1,
				DirNitNom2,DirNitApe1,DirNitApe2,DirNitCon,DirNitDir,DirNitTel,DirNitFax,
				DirNitEmail,DepCod,MunCod,BarCod,DirNitEst,DirNitReg,DirNitFax,DirNitPro,
				DirNitTel,DirNitAut,DirNitGra,DirNitRetIca,DirNitPorRetIca,DirNitCli,
				DirNitCliTip,DirNitCliCup,DirNitEmp,DirNitSoc,DirPreCom,DirNumCom,
				ZonCod,DirZonaEspecial,DirNitSobregiro,DirNitListaPrecios,DirNitVendedor,
				DirNitForPag,DirNitDirEntrega,DirNaturaleza,DirActEconomica,DirCodPais,
				DirDeclarante 
				FROM DIRECTORIONITS 
				WHERE  DirNitSobregiro=1
				AND empresaid=".$idEmpresa;
				$datos=$this->conector->select($sql);
		$arr=array();
		for ($i=0; $i <count($datos) ; $i++) {
			$sql="	UPDATE DIRECTORIONITS 
					SET DirNitSobregiro=0 
					WHERE DirNitId=".$datos[$i][1]." 
					AND EmpresaId=".$idEmpresa;
			$this->conector->update($sql);
			   $arr[]=array(
					// 'EmpresaId' =>  trim(utf8_encode($datos[$i][0])),
					 'codigo' =>  trim(utf8_encode($datos[$i][1])),
					 'docIde' =>  trim(utf8_encode($datos[$i][1])),
					 //'DirNitDigVer' =>  trim(utf8_encode($datos[$i][2])),
					 'tipoDoc' =>  trim(utf8_encode($datos[$i][3])),
					 'razonSoc' =>trim(utf8_encode($datos[$i][4])),
					 'nombre1' =>  trim(utf8_encode($datos[$i][5])),
					 'nombre2' =>  trim(utf8_encode($datos[$i][6])),
					 'apellido1' =>  trim(utf8_encode($datos[$i][7])),
					 'apellido2' =>  trim(utf8_encode($datos[$i][8])),
					 'nomCont' =>  trim(utf8_encode($datos[$i][9])),
					 'direccion' =>  trim(utf8_encode($datos[$i][10])),
					 'telFijo' =>  trim(utf8_encode($datos[$i][11])),
					 'telCel' =>  trim(utf8_encode($datos[$i][12])),
					 'email' =>  trim(utf8_encode($datos[$i][13])),
					 'codDep' =>  trim(utf8_encode($datos[$i][14])),
					 'codMun' =>  trim(utf8_encode($datos[$i][15])),
					 'codBar' =>  trim(utf8_encode($datos[$i][16])),
					 'estado' =>  trim(utf8_encode($datos[$i][17])),
					// 'DirNitReg' =>  trim(utf8_encode($datos[$i][18])),
					// 'DirNitFax' =>  trim(utf8_encode($datos[$i][19])),
					// 'DirNitPro' =>  trim(utf8_encode($datos[$i][20])),
					// 'DirNitTel' =>  trim(utf8_encode($datos[$i][21])),
					// 'DirNitAut' =>  trim(utf8_encode($datos[$i][22])),
					 'tipoCon' =>  trim(utf8_encode($datos[$i][23])),
					 //'DirNitRetIca' =>  trim(utf8_encode($datos[$i][24])),
					 //'DirNitPorRetIca' =>  trim(utf8_encode($datos[$i][25])),
					 'estCliente' =>  trim(utf8_encode($datos[$i][26])),
					 'nivel' =>  trim(utf8_encode($datos[$i][27])),
					 'cupo' =>  trim(utf8_encode($datos[$i][28])),
					 'vinculo' =>  trim(utf8_encode($datos[$i][29])),
					 'rol' =>  trim(utf8_encode($datos[$i][30])),
					 //'DirPreCom' =>  trim(utf8_encode($datos[$i][31])),
					 //'DirNumCom' =>  trim(utf8_encode($datos[$i][32])),
					 'zona' =>  trim(utf8_encode($datos[$i][33])),
					 'zonaEsp' =>  trim(utf8_encode($datos[$i][34])),
					 //'DirNitSobregiro' =>  trim(utf8_encode($datos[$i][35])),
					 //'DirNitListaPrecios' =>  trim(utf8_encode($datos[$i][36])),
					 'docVen' =>  trim(utf8_encode($datos[$i][37])),
					 'formasPago' =>  trim(utf8_encode($datos[$i][38])),
					 'lugarEnt' =>  trim(utf8_encode($datos[$i][39])),
					 'clasePer' =>  trim(utf8_encode($datos[$i][40])),
					 'actEco' =>  trim(utf8_encode($datos[$i][41])),
					 //'DirCodPais' =>  trim(utf8_encode($datos[$i][42])),
					 'decRenta' =>  trim(utf8_encode($datos[$i][43]))
				 );
		}
		return $arr;
	}

	public function RegistrarFacturasCredito($jsonDataEncoded){
	    $ch = curl_init();
	    $header = array();
	    $header[] = 'Content-length: '.strlen($jsonDataEncoded);
	    $header[] = 'Content-Type: application/json';
	    $header[] = 'Authorization: '.Variables::$AUTHORIZATIONXEO;
	    curl_setopt( $ch, CURLOPT_HTTPHEADER, $header );
	    curl_setopt( $ch, CURLOPT_URL, Variables::$URLXEO."RegistrarFacturasCredito" );
		  curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
	    curl_setopt( $ch, CURLOPT_POSTFIELDS,  $jsonDataEncoded);
	    curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
	    $server_output = curl_exec ( $ch );
	    curl_close ( $ch );
	    return $server_output;
	}
	
   	public function migrar_facturas_credito($idEmpresa){
		$sql="	SELECT  co.empresaid,co.ComNum,ComNit,co.ComCambio
				FROM comprobante co, cuenta cu
				WHERE co.ComNum = cu.CarNum
				AND co.empresaid=cu.empresaid
				AND co.tipcomid=cu.tipcomid
				AND co.empresaid=$idEmpresa
				AND ( co.ComCambio is null or co.ComCambio !=0 ) ";
		$datos=$this->conector->select($sql);
		return $datos;
	}

}

?>
